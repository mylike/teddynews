﻿
USE TeddyNews;

IF OBJECT_ID('AccountCurrentArticle') IS NOT NULL DROP TABLE AccountCurrentArticle;
IF OBJECT_ID('AccountTaste') IS NOT NULL DROP TABLE AccountTaste;
IF OBJECT_ID('Opinion') IS NOT NULL DROP TABLE Opinion;
IF OBJECT_ID('ArticleTag') IS NOT NULL DROP TABLE ArticleTag;
IF OBJECT_ID('Tag') IS NOT NULL DROP TABLE Tag;
IF OBJECT_ID('Article') IS NOT NULL DROP TABLE Article;
IF OBJECT_ID('Account') IS NOT NULL DROP TABLE Account;
IF OBJECT_ID('Spider') IS NOT NULL DROP TABLE Spider;



CREATE TABLE Account
(
	Id UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,	
	IsAuthenticated BIT NOT NULL,
	CurrentArticleDate DATETIME NOT NULL DEFAULT GETDATE()
);

CREATE TABLE Article
(
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Title NVARCHAR(256) NOT NULL,
	Url VARCHAR(1024) NOT NULL,
	UrlComment VARCHAR(1024) NULL,--For HackerNews comment page
	DateTimeStamp DATETIME NOT NULL DEFAULT(GETUTCDATE()),
	InitialRating INT NOT NULL DEFAULT(3),	
	NbLike INT NOT NULL DEFAULT(0),	
	NbDislike INT NOT NULL DEFAULT(0),
);

CREATE TABLE Tag
(
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Name VARCHAR(128) NOT NULL,
);
CREATE UNIQUE INDEX IX_Tag_Name ON Tag(Name);

CREATE TABLE ArticleTag
(
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	ArticleId INT NOT NULL REFERENCES Article(Id),
	TagId INT NOT NULL REFERENCES Tag(Id),
	Score FLOAT NOT NULL DEFAULT(3), -- IMDB like: 8 to 10 is a must read whatever you like the style. 5 is ony if you are fan.
);

CREATE TABLE Opinion
(
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	AccountId UNIQUEIDENTIFIER NOT NULL REFERENCES Account(Id),
	ArticleId INT NOT NULL REFERENCES Article(Id),
	Visited BIT NOT NULL DEFAULT(0),
	Skipped BIT NOT NULL DEFAULT(0),
	Liked BIT NOT NULL DEFAULT(0),
	Disliked BIT NOT NULL DEFAULT(0),	
);
CREATE UNIQUE INDEX IX_Opinion_ArticleAccount ON Opinion(AccountId, ArticleId);

CREATE TABLE AccountTaste
(
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	AccountId UNIQUEIDENTIFIER NOT NULL REFERENCES Account(Id),
	TagId INT NOT NULL REFERENCES Tag(Id),
	Coeficient FLOAT NOT NULL	
);
CREATE UNIQUE INDEX IX_AccountTaste_ArticleTag ON AccountTaste(AccountId, TagId);


CREATE TABLE AccountCurrentArticle
(
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	AccountId UNIQUEIDENTIFIER NOT NULL REFERENCES Account(Id),
	ArticleId INT NOT NULL REFERENCES Article(Id),	
);
CREATE UNIQUE INDEX IX_AccountCurrentArticle_ArticleAccount ON AccountCurrentArticle(AccountId, ArticleId);

GO

-- Create demo data
INSERT INTO Article (Title, Url) VALUES
	('What If We Could Weaponize Empathy?', 'http://blog.codinghorror.com/what-if-we-could-weaponize-empathy/'),
	('Amazon Echo', 'http://www.amazon.com/oc/echo'),
	('Doing Business in Japan ', 'http://www.kalzumeus.com/2014/11/07/doing-business-in-japan/'),
	('Sometimes, it’s just time to go home', 'http://www.benmilne.com/sometimes-just-go-home/'),
	('Computer Scientists Ask Supreme Court to Rule APIs Can’t Be Copyrighted', 'https://www.eff.org/press/releases/computer-scientists-ask-supreme-court-rule-apis-cant-be-copyrighted'),
	('Pulling JPEGs out of thin air', 'http://lcamtuf.blogspot.com/2014/11/pulling-jpegs-out-of-thin-air.html'),
	('My kind of Hybrid!', 'http://9gag.com/gag/a49PM0y'),
	('Linux kernel coding style ', 'http://9gag.com/gag/aDw7ZbZ'),
	('Virtual reality Jobs at Apple', 'http://9gag.com/gag/aDw7ZbZ'),
	('Solving the Mystery of Link Imbalance: A Metastable Failure State at Scale', 'http://9gag.com/gag/aDw7ZbZ'),
	('The Increasing Trend of Online Extortion', 'http://9gag.com/gag/aDw7ZbZ'),
	('How an EBay Bookseller Defeated a Publishing Giant at the Supreme Court ', 'http://9gag.com/gag/aDw7ZbZ'),
	('Lobster is a game programming language', 'http://9gag.com/gag/aDw7ZbZ'),
	('The First 3-D Printer in Space Makes Its First Object: A Spare Part', 'http://9gag.com/gag/aDw7ZbZ'),
	('Alexander Grothendieck – A Country Known Only by Name', 'http://9gag.com/gag/aDw7ZbZ'),
	('Internet companies should not be monitoring terrorists or anyone else', 'http://9gag.com/gag/aDw7ZbZ'),
	('Java Garbage Collection Distilled (2013)', 'http://9gag.com/gag/aDw7ZbZ'),
	('Why HTTPS Everywhere isn''t on addons.mozilla.org ', 'http://9gag.com/gag/aDw7ZbZ'),
	('Show HN: Console Driven Dating for Programmers', 'http://9gag.com/gag/aDw7ZbZ'),
	('HSBC, Goldman Rigged Metals’ Prices for Years, Suit Says', 'http://9gag.com/gag/aDw7ZbZ'),
	('Teespring (YC W13) Is Looking for Senior UI/Front-end Engineers', 'http://9gag.com/gag/aDw7ZbZ'),
	('God''s Lonely Programmer', 'http://9gag.com/gag/aDw7ZbZ'),
	('Should the U.S. Make Standardized Tests Harder?', 'http://9gag.com/gag/aDw7ZbZ'),
	('The University of Florida Sparse Matrix Collection', 'http://9gag.com/gag/aDw7ZbZ'),
	('Show HN: Event Horizon – CQRS/ES Toolkit for Go', 'http://9gag.com/gag/aDw7ZbZ'),
	('A Use for Big Data: Cracking the Voynich Manuscript', 'http://9gag.com/gag/aDw7ZbZ'),
	('How we built Flow', 'http://9gag.com/gag/aDw7ZbZ'),
	('Friendsgiving, a new tradition to be thankful for', 'http://9gag.com/gag/aDw7ZbZ'),
	('The Problem With International Development and a Plan to Fix It', 'http://9gag.com/gag/aDw7ZbZ'),
	('St – simple terminal', 'http://9gag.com/gag/aDw7ZbZ'),
	('Maze solving using fatty acid chemistry', 'http://9gag.com/gag/aDw7ZbZ'),
	('Vladimir Vapnik Joins Facebook Research', 'http://9gag.com/gag/aDw7ZbZ'),
	('Introducing Driver Destination', 'http://9gag.com/gag/aDw7ZbZ'),
	('A “Complex” Theory of Consciousness', 'http://9gag.com/gag/aDw7ZbZ'),
	('Antialiasing: To Splat or Not', 'http://9gag.com/gag/aDw7ZbZ')
	;

INSERT INTO Tag (Name) VALUES
	('HN'),	
	('9GAG');

INSERT INTO ArticleTag (ArticleId, TagId) SELECT Id, 1 FROM Article WHERE Id % 2 = 0;
INSERT INTO ArticleTag (ArticleId, TagId) SELECT Id, 2 FROM Article WHERE Id % 2 = 1;

UPDATE Article SET InitialRating=8, UrlComment='http://www.kalzumeus.com/2014/11/07/doing-business-in-japan/' WHERE Id = 3;
