﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
	public partial class Tag
	{
		public override string ToString()
		{
			return this.Name;
		}
	}
	public partial class Article
	{
		public override string ToString()
		{
			return this.Title;
		}		
	}

	public partial class Account
	{
		public override string ToString()
		{
			return this.Id.ToString();
		}
	}
}
