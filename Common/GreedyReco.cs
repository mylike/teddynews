﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;


namespace Common
{
	/// <summary>
	/// Scoring stuff
	/// </summary>
    public static class GreedyReco
    {
		private static log4net.ILog log = log4net.LogManager.GetLogger(typeof(GreedyReco));
		private static Random rand = new Random();


		public static async Task CheckViewTime(Model db, Account account)
		{
			if (DateTime.UtcNow.Subtract(account.CurrentArticleDate).TotalHours > 2)
			{
				log.Info("CheckViewTime: clean current articles for account: " + account);
				foreach (AccountCurrentArticle accountCurrentArticle in account.AccountCurrentArticles.ToArray())
				{
					db.Entry(accountCurrentArticle).State = EntityState.Deleted;
				}
				await db.SaveChangesAsync();

			}
		}

		public static async Task RefreshCurrentArticles(Model db, Account account)
		{

			int nbArticle = 16 - account.AccountCurrentArticles.Count;
			if (nbArticle <= 0)
			{
				return;
			}
			log.Info("RefreshCurrentArticle for " +  nbArticle + " articles for account: " + account);
			account.CurrentArticleDate = DateTime.UtcNow;
			await db.SaveChangesAsync();
			List<Tag> allTags = await db.Tags.ToListAsync();
			for (int i = 0; i < nbArticle; i++)
			{
				await RecommandOneArticle(db, account, allTags);
			}
		}
		public static async Task RecommandOneArticle(Model db, Account account, List<Tag> allTags)
		{			
			Tag tag = null;
			if (allTags.Count >= 1)
			{
				if (rand.Next(100) < 95)
				{
					///Look on feed user love
					tag = GetFavoriteTag(allTags, account);
				}
				if (tag == null)
				{
					///dicover a tag
					tag = allTags[rand.Next(allTags.Count)];
				}
				else
				{
					///Take best article over all tags.
				}
			}
			DateTime recentDate2 = DateTime.UtcNow.AddDays(-2);
			DateTime recentDate1 = DateTime.UtcNow.AddDays(-14);
			
			Article article = null;
			if (tag != null)
			{
				article = await db.ArticleTags
					.Where(at => at.TagId == tag.Id)
					.Select(at => at.Article)
					.Where(a => !a.Opinions.Any(o => o.AccountId == account.Id))
					.Where(a => !a.AccountCurrentArticles.Any(c => c.AccountId == account.Id))
					.OrderByDescending(t =>
						t.InitialRating +
						(t.DateTimeStamp > recentDate2 ? 2 : t.DateTimeStamp > recentDate1 ? 1 : 0))
						.FirstOrDefaultAsync();

			}
			if (article == null)
			{
				article = await db.ArticleTags
					.Select(at => at.Article)
					.Where(a => !a.Opinions.Any(o => o.AccountId == account.Id))
					.Where(a => !a.AccountCurrentArticles.Any(c => c.AccountId == account.Id))
					.OrderByDescending(t =>
						t.InitialRating +
						(t.DateTimeStamp > recentDate2 ? 2 : t.DateTimeStamp > recentDate1 ? 1 : 0))
						.FirstOrDefaultAsync();
			}
			if (article == null)
			{
				log.Error("No more article for " + account + ", tag: " + (tag == null ? "null" : tag.Name));
				return;
			}
			account.AccountCurrentArticles.Add(
				new AccountCurrentArticle
				{
					Article = article,
				});

			await db.SaveChangesAsync();
		}

		private static Tag GetFavoriteTag(List<Tag> allTags, Account account)
		{
			
			var tagTable = new List<Tuple<Tag, int>>();
			int currentScore = 0;
			foreach (Tag tag in allTags)
			{
				currentScore += 0;
				AccountTaste taste = account.AccountTastes.FirstOrDefault(at => at.TagId == tag.Id);
				if (taste != null)
				{
					currentScore += (int) Math.Round(Math.Max(0, Math.Min(97, taste.Coeficient)));
				}
				tagTable.Add(Tuple.Create(tag, currentScore));
			}
			if (currentScore < 3)
			{
				///Have at least 3 results to start using favorites
				return null;
			}
			int r = GreedyReco.rand.Next(currentScore);
			foreach (var tuple in tagTable)
			{
				if (r < tuple.Item2)
				{
					return tuple.Item1;
				}
			}
			return null;
		}

		//public static Tuple<string, string, string> ExplainReco(Account account, Article article)
		//{
		//	double totalScore = getTotalScore(account, article, DateTime.UtcNow);
		//	double tagScore =  getTagScore(account, article);
		//	double timeScore = getTimeScore(article, DateTime.UtcNow);
		//	double teddyLikeScore = getTeddyLikeScore(article);

		//	string scoreString = ((int) Math.Round(totalScore)).ToString("+0;-0");
		//	string bonusString;
		//	string bonusExplaination;
		//	int bonus = (int) Math.Round(tagScore + timeScore + teddyLikeScore);
		//	if (bonus >= 5)
		//	{
		//		bonusString = "+5";				
		//	}
		//	else if (bonus < -5)
		//	{
		//		bonusString = "-5";
		//	}
		//	else
		//	{
		//		bonusString = bonus.ToString("+0;-0");
		//	}
			
		//	bonusExplaination = "Secret ingredient";			
		//	if (bonus >= 2)
		//	{
		//		if (tagScore > timeScore && tagScore > teddyLikeScore)
		//		{
		//			bonusExplaination = "The article looks like your favorites";
		//		}
		//		else if (timeScore > tagScore && timeScore > teddyLikeScore)
		//		{
		//			bonusExplaination = "The article is recent";
		//		}
		//		else if (teddyLikeScore > tagScore && teddyLikeScore > timeScore)
		//		{
		//			bonusExplaination = "The article is recommended by readers";
		//		}
		//	}
		//	else if (bonus <= -2)
		//	{
		//		if (tagScore < timeScore && tagScore < teddyLikeScore)
		//		{
		//			bonusExplaination = "The article is far form what you have in your favorites";
		//		}
		//		else if (timeScore < tagScore && timeScore < teddyLikeScore)
		//		{
		//			bonusExplaination = "The article is old";
		//		}
		//		else if (teddyLikeScore < tagScore && teddyLikeScore < timeScore)
		//		{
		//			bonusExplaination = "The article is disliked by readers";
		//		}
		//	}
		//	return Tuple.Create(scoreString, bonusString, bonusExplaination);
		//}

		//public static async Task<List<Tuple<Article, Opinion>>> GetFirstTimeArticles(Model db)
		//{
		//	List<Article> articles = await db.Articles				
		//		.Include(a => a.ArticleTags.Select(at => at.Tag)).ToListAsync();
		//	return articles
		//		.OrderByDescending(a => getTotalScore(null, a, DateTime.Now))
		//		.Take(7).Select(a => new Tuple<Article, Opinion>(a, null)).ToList();
		//}
		
		//private static double getTagScore(Account account, Article article)
		//{
		//	double sum = 0;
		//	var tasteDico = account.AccountTastes.ToDictionary(at => at.Tag, at => at.Coeficient);
		//	foreach (Tag tag in article.ArticleTags.Select(at => at.Tag))
		//	{
		//		if (tasteDico.ContainsKey(tag))
		//		{
		//			sum += tasteDico[tag];
		//		}
		//	}
		//	return sum;
		//}


		//private static double getTotalScore(Account account, Article article, DateTime now)
		//{
		//	double tagScore = account != null ? getTagScore(account, article) : 0;
		//	double timeScore = getTimeScore(article, now);
		//	double ratingScore = article.InitialRating;
		//	double teddyLikeScore = getTeddyLikeScore(article);
		//	double totalScore = ratingScore + tagScore + timeScore + teddyLikeScore;
		//	return totalScore;
		//}


		//private static double getTagScoreDiscovery(Account account, Article article, List<Article> displayedArticles)
		//{
		//	double sum = 0;
		//	Dictionary<Tag, double> tasteDico = account == null ?
		//		new Dictionary<Tag, double>() :
		//		account.AccountTastes.ToDictionary(at => at.Tag, at => at.Coeficient);
		//	foreach (Article visibleArticle in displayedArticles)
		//	{
		//		foreach (Tag tag in visibleArticle.ArticleTags.Select(at => at.Tag))
		//		{
		//			if (tasteDico.ContainsKey(tag))
		//			{
		//				if (tasteDico[tag] > 0)
		//				{
		//					tasteDico[tag] = 0.75 * tasteDico[tag];
		//				}
		//			}
		//			else
		//			{
		//				tasteDico[tag] = -0.5;///Force to present article on different themes
		//			}
		//		}
		//	}
		//	foreach (Tag tag in article.ArticleTags.Select(at => at.Tag))
		//	{
		//		if (tasteDico.ContainsKey(tag))
		//		{
		//			sum += tasteDico[tag];
		//		}
		//	}
		//	return sum;
		//}

		//private static double getTotalDiscoveryScore(Account account, Article article, DateTime now, List<Article> displayedArticles)
		//{
		//	double tagScore = getTagScoreDiscovery(account, article, displayedArticles);
		//	double ratingScore = article.InitialRating;
		//	double teddyLikeScore = getTeddyLikeScore(article);
		//	double totalScore = ratingScore + tagScore + teddyLikeScore;///Ignore timeScore, choose best of all time
		//	return totalScore;
		//}


		//private static double getTimeScore(Article article, DateTime now)
		//{
		//	if (now.Subtract(article.DateTimeStamp).TotalDays <= 1)
		//	{
		//		return 2;
		//	}
		//	else if (now.Subtract(article.DateTimeStamp).TotalDays <= 2)
		//	{
		//		return 1;
		//	}
		//	else if (now.Subtract(article.DateTimeStamp).TotalDays <= 7)
		//	{
		//		return 0;
		//	}
		//	else if (now.Subtract(article.DateTimeStamp).TotalDays <= 21)
		//	{
		//		return -1;
		//	}
		//	return -2;
		//}

		//private static double getTeddyLikeScore(Article article)
		//{
		//	if (article.NbLike == 0 && article.NbDislike == 0)
		//	{
		//		return 0;
		//	}
		//	if (article.NbLike > 5 * article.NbDislike)
		//	{
		//		return 5;
		//	}
		//	if (article.NbLike > 2 * article.NbDislike)
		//	{
		//		return 2;
		//	}
		//	if (article.NbLike > article.NbDislike)
		//	{
		//		return 1;
		//	}
		//	if (article.NbDislike > 3 * article.NbLike)
		//	{
		//		return -5;
		//	}
		//	if (article.NbDislike > article.NbLike)
		//	{
		//		return -2;
		//	}
		//	return 0;
		//}
        
		private static void applyTaste(Account account, Article article, double bonus)
        {
            if (article.ArticleTags.Count == 0)
            {
                return;
            }         
            var tasteDico = account.AccountTastes.ToDictionary(at => at.Tag, at => at);
            foreach (Tag tag in article.ArticleTags.Select(at => at.Tag))
            {
                if (tasteDico.ContainsKey(tag))
                {
					tasteDico[tag].Coeficient = Math.Max(-5000, Math.Min(5000, tasteDico[tag].Coeficient + bonus));					
                }
                else
                {
					AccountTaste at = new AccountTaste { Account = account, Tag = tag, Coeficient = bonus };
                    account.AccountTastes.Add(at);
                }
            }
        }

        public static async Task OpinionUpdated(Model db, Opinion opinion)
        {
			Article article = await db.Articles
				.Include(a => a.ArticleTags.Select(at => at.Tag))
				.Include(a => a.Opinions)
				.FirstOrDefaultAsync(a => a.Id == opinion.ArticleId);
			double bonus;
			if (opinion.Visited)
            {
				bonus = 1;
            }
			else if (opinion.Skipped)
			{
				bonus = -0.01;
			}
			else
			{
				bonus = 0;
			}
			if (bonus != 0)
			{
				applyTaste(opinion.Account, opinion.Article, bonus);
			}
        }
    }
}
