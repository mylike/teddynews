﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{

	/// <summary>
	/// Advanced AccountTaste
	/// </summary>
	public class AccountPredictor
	{
		private Model model;
		private Account account;
		public readonly List<ClausePredictor> ClauseList = new List<ClausePredictor>();
		private Dictionary<ClausePredictor, double> initialCoeficients = new Dictionary<ClausePredictor,double>();


		public AccountPredictor(Model model, Account account)
		{
			this.model = model;
			this.account = account;
			foreach (AccountTaste taste in this.account.AccountTastes)
			{
				if (taste.Coeficient != 0)
				{
					this.ClauseList.Add(new ClausePredictor { Tag = taste.Tag, Test = PreditorTest.TagPresent, Coeficient = taste.Coeficient });
				}
				if (taste.ScoreIfMissing != 0)
				{
					this.ClauseList.Add(new ClausePredictor { Tag = taste.Tag, Test = PreditorTest.TagMissing, Coeficient = taste.ScoreIfMissing });
				}
			}

			foreach (ClausePredictor c in this.ClauseList)
			{
				initialCoeficients[c] = c.Coeficient;
			}
		}

		public void Reset(ClausePredictor clause)
		{
			if (!this.initialCoeficients.ContainsKey(clause))
			{
				this.ClauseList.Remove(clause);
			}
			else
			{
				clause.Coeficient = this.initialCoeficients[clause];
			}
		}

		public void Save()
		{
			this.account.AccountTastes.Clear();
            foreach (var item in convertToAccountTasteDico().Values)
            {
                this.account.AccountTastes.Add(item);
            }
		}
		

		/// <summary>
		/// 1 => like
		/// 0.5 => view
		/// 0 => not view
		/// -1 => dislike
		/// </summary>
		public double PredictScore(Article article)
		{
			double r = 0;
			foreach (ClausePredictor c in this.ClauseList)
			{
				if (c.Test == PreditorTest.TagPresent)
				{
					if (this.model.ArticleTagExist(article, c.Tag))
					{
						r += c.Coeficient;
					}
				}
				else if (c.Test == PreditorTest.TagMissing)
				{
					if (!this.model.ArticleTagExist(article, c.Tag))
					{
						r += c.Coeficient;
					}
				}
				else
				{
					throw new NotImplementedException();
				}
			}
			return r;
		}

		private Dictionary<Tag, AccountTaste> convertToAccountTasteDico()
		{
			Dictionary<Tag, AccountTaste> dico = new Dictionary<Tag,AccountTaste>();
			foreach (ClausePredictor c in this.ClauseList)
			{
				if(Math.Abs(c.Coeficient)<double.Epsilon)
				{
					continue;
				}
				if (!dico.ContainsKey(c.Tag))
				{
					dico.Add(c.Tag, new AccountTaste { Tag = c.Tag });
				}
				switch (c.Test)
				{
					case PreditorTest.TagPresent: dico[c.Tag].Coeficient = c.Coeficient; break;
					case PreditorTest.TagMissing: dico[c.Tag].ScoreIfMissing = c.Coeficient; break;
					default: throw new NotImplementedException();
				}
			}
			return dico;
		}

		public string[] DebugString()
		{
			return this.convertToAccountTasteDico()
				.OrderByDescending(p => p.Value.Coeficient)
				.Select(p => p.Key.Name + ": " + p.Value.Coeficient.ToString("0.00") + (p.Value.ScoreIfMissing != 0 ? " / (-)" + p.Value.ScoreIfMissing.ToString("0.00") : ""))
				.ToArray();
		}

	}

	public enum PreditorTest { TagPresent, TagMissing };
	public class ClausePredictor
	{
		public Tag Tag;
		public PreditorTest Test;		
		public double Coeficient;
	}
}
