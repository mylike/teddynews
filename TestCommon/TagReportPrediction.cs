﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class TagReportPrediction
    {
        private static Random rand = new Random();
        private Model db;
        private Account account;
        private Dictionary<Tag, TagStat> tagStatList;
        private readonly double goodRating;
        private readonly double badRating;
        
        public TagReportPrediction(Model db, Account account)
        {
            this.db = db;
            this.account = account;
            List<Article> articleList = db.Articles.Values.OrderByDescending(a => a.Rating).ToList();
            this.goodRating = articleList[articleList.Count / 3].Rating;
            this.badRating = articleList[articleList.Count * 2 / 3].Rating;

            this.initGeneralTagStatList();
        }

        private void initGeneralTagStatList()
        {
            this.tagStatList = new Dictionary<Tag, TagStat>();
            foreach (Tag tag in db.Tags.Values)
            {
                TagStat stat = new TagStat { Tag = tag };
                tagStatList[tag] = stat;
            }            
            foreach (Article article in db.Articles.Values)
            {
                foreach (TagStat stat in tagStatList.Values)
                {
                    TagSetPerFilter presentOrMissing = stat.GetPresentOrMissing(db.ArticleTagExist(article, stat.Tag));
                    if (article.Rating > this.goodRating)
                    {
                        presentOrMissing.GeneralOnAllArticles.Like++;
                    }
                    else if (article.Rating < this.badRating)
                    {
                        presentOrMissing.GeneralOnAllArticles.Disliked++;
                    }
                    else
                    {
                        presentOrMissing.GeneralOnAllArticles.Visited++;
                    }
                }
            }
            foreach (TagStat stat in tagStatList.Values)
            {
                stat.Refresh();
            }
        }

        public void ImprovePrediction()
        {
            foreach (TagStat stat in tagStatList.Values)
            {
                stat.CleanForAccount();
            }
            foreach (Opinion opinion in this.account.Opinions)
            {
                Article article = opinion.Article;
                foreach (TagStat stat in tagStatList.Values)
                {
                    TagSetPerFilter presentOrMissing = stat.GetPresentOrMissing(db.ArticleTagExist(article, stat.Tag));
                    if (opinion.Liked)
                    {
                        presentOrMissing.Account.Like++;
                    }
                    else if (opinion.Disliked)
                    {
                        presentOrMissing.Account.Disliked++;
                    }
                    else if (opinion.Visited)
                    {
                        presentOrMissing.Account.Visited++;
                    }
                    else
                    {
                        continue;///This opinion is ignored for 'known article'
                    }
                    if (article.Rating > this.goodRating)
                    {
                        presentOrMissing.GeneralOnKnownArticles.Like++;
                    }
                    else if (article.Rating < this.badRating)
                    {
                        presentOrMissing.GeneralOnKnownArticles.Disliked++;
                    }
                    else
                    {
                        presentOrMissing.GeneralOnKnownArticles.Visited++;
                    }
                }
            }
            foreach (TagStat stat in tagStatList.Values)
            {
                stat.Refresh();
            }
        }

        public TagStatWeight PredictScore(Article article)
        {
            TagStatWeight bestWeight = null;           
            foreach (TagStat stat in tagStatList.Values)
            {
                bool isPresentOrMissing = this.db.ArticleTagExist(article, stat.Tag);
                foreach(TagStatWeight result in stat.GetTagStatWeightForCategories(isPresentOrMissing))
                {
                    if (bestWeight == null || result.CompareTo(bestWeight) > 0)
                    {
                        bestWeight = result;                   
                    }                
                }
            }
            return bestWeight;          
        }


        private List<TagStatWeight> debugGetBestPredictScore(Article article, int nbResults)
        {
            List<TagStatWeight> allWeight = new List<TagStatWeight>();
            foreach (TagStat stat in tagStatList.Values)
            {
                bool isPresentOrMissing = this.db.ArticleTagExist(article, stat.Tag);
                foreach (TagStatWeight result in stat.GetTagStatWeightForCategories(isPresentOrMissing))
                {
                    allWeight.Add(result);
                }
            }
            return allWeight.OrderByDescending(w => w).Take(nbResults).ToList();
        }
        public string DebugDisplay(Article selectedArticle)
        {
            string r = "";

			r += debugDisplayForCategory(1) + Environment.NewLine + Environment.NewLine;
			r += debugDisplayForCategory(0) + Environment.NewLine + Environment.NewLine;
			r += debugDisplayForCategory(-1) + Environment.NewLine + Environment.NewLine;
            
            if (selectedArticle != null)
            {
                List<TagStatWeight> bestWeight = debugGetBestPredictScore(selectedArticle, 50);
				r += Environment.NewLine + Environment.NewLine;
                r += "Filter for selected ARTICLE " + selectedArticle.Title + Environment.NewLine +
                    debugDisplayArray(bestWeight) + Environment.NewLine;
            }
            return r;
        }

        private string debugDisplayForCategory(int category)
        {
            TagStatWeight[] top = getTopTagStatWeightForCategory(category, 7);
            return "Main filter for " + (category == 1 ? "LIKE" : (category == -1 ? "DISLIKE" : "VISITED")) + Environment.NewLine +
                debugDisplayArray(top);               
        }

        private TagStatWeight[] getTopTagStatWeightForCategory(int category, int takeCount)
        {
            List<TagStatWeight> all = new List<TagStatWeight>();
            foreach (TagStat stat in this.tagStatList.Values.OrderByDescending(stat => stat.Present.GeneralOnKnownArticles.Total))
            {
                all.Add(stat.GetTagStatWeight(true, category));
                all.Add(stat.GetTagStatWeight(false, category));
            }
            TagStatWeight[] top = all.OrderByDescending(w => w).Take(takeCount).ToArray();
            return top;
        }

        private static string debugDisplayArray(IEnumerable<TagStatWeight> list)
        {
            return "L " + "Tag".PadRight(32) + " Rate P   Seen  Total" + Environment.NewLine +
                string.Join(Environment.NewLine, list.Select(w =>
                     w.category.ToString("+;-;0") +
                     " " + ((w.isArticlePresent ? "" : " (-)") + w.TagStat.Tag.Name).PadRight(32) +
                     " " + w.ResultAccuracy.ToString("0.00") +
                     " " + w.CompArticlePresent +
                     " " + w.AccountTotal.ToString("#,0").PadLeft(6) +
                     " " + w.TagTotal.ToString("#,0").PadLeft(6)
                     ).ToArray());
        }

        public List<Article> GetNextArticlesForRefine()
        {
            HashSet<Article> alreadyView = new HashSet<Article>(this.account.Opinions.Select(o => o.Article));
            
            HashSet<Tag> like = new HashSet<Tag>(this.getTopTagStatWeightForCategory(1, 5).Select(w =>w.TagStat.Tag));
            HashSet<Tag> visited = new HashSet<Tag>(this.getTopTagStatWeightForCategory(0, 5).Select(w => w.TagStat.Tag));
            HashSet<Tag> dislike = new HashSet<Tag>(this.getTopTagStatWeightForCategory(-1, 5).Select(w => w.TagStat.Tag));

            HashSet<Tag> dislikeOrVisited = new HashSet<Tag>();
            dislikeOrVisited.UnionWith(visited);
            dislikeOrVisited.UnionWith(dislike);
			//List<Article> returned = new List<Article>();           
			//foreach (Article article in this.db.Articles.Values.OrderByDescending(a => a.NumberOfView))
			//{
			//	if (alreadyView.Contains(article))
			//	{
			//		continue;
			//	}
			//	if (numberOfMatch(article, like) >= 1 && numberOfMatch(article, dislikeOrVisited) >= 1)
			//	{
			//		returned.Add(article);
			//		alreadyView.Add(article);
			//		if (returned.Count >= 7)
			//		{
			//			break;
			//		}
			//	}
			//}
           // return returned;
			List<Article> notSeenMovies = this.db.Articles.Values
				.Where(a => !alreadyView.Contains(a))
				.OrderByDescending(a => a.NumberOfView).ToList();
			int minNumberOfView = notSeenMovies[notSeenMovies.Count / 2].NumberOfView??0;
			return notSeenMovies
				.Where(a => a.NumberOfView > minNumberOfView)
				.OrderByDescending(a => scoreForRefine(a, like, visited, dislike))
				.Take(7)
				.ToList();
        }
		private int scoreForRefine(Article article, HashSet<Tag> like, HashSet<Tag> visited, HashSet<Tag> dislike)
		{
			int matchLike = numberOfMatch(article, like);
			int matchVisited = numberOfMatch(article, visited);
			int matchDislike = numberOfMatch(article, dislike);
			return matchLike;// *(matchVisited + matchDislike);
		}
        private int numberOfMatch(Article article,  HashSet<Tag> tags)
        {
            int r = 0;
            foreach (Tag tag in article.ArticleTags.Select(at => at.Tag))
            {
                if (tags.Contains(tag))
                {
                    r++;
                }
            }
            return r;
        }
    }

    public class TagStat
    {
        public Tag Tag;
        public readonly TagSetPerFilter Present = new TagSetPerFilter();
        public readonly TagSetPerFilter Missing = new TagSetPerFilter();

        /// <summary>
        /// this.Weigts[presentOrMissing][1+ category]
        /// </summary>
        public TagStatWeight[][] Weigts = new[] { new TagStatWeight[3], new TagStatWeight[3] };
        public void CleanForAccount()
        {
            this.Present.CleanForAccount();
            this.Missing.CleanForAccount();
        }

        public void Refresh()
        {
            this.Present.Refresh();
            this.Missing.Refresh();

            this.Weigts[0][0] = new TagStatWeight(this, -1, true);
            this.Weigts[0][1] = new TagStatWeight(this, 0, true);
            this.Weigts[0][2] = new TagStatWeight(this, +1, true);
            this.Weigts[1][0] = new TagStatWeight(this, -1, false);
            this.Weigts[1][1] = new TagStatWeight(this, 0, false);
            this.Weigts[1][2] = new TagStatWeight(this, +1, false);
        }

        public TagStatWeight GetTagStatWeight(bool isArticlePresent, int category)
        {
            return this.Weigts[isArticlePresent ? 0 : 1][category + 1];
        }

        public IEnumerable<TagStatWeight> GetTagStatWeightForCategories(bool isArticlePresent)
        {
            return this.Weigts[isArticlePresent ? 0 : 1];
        }
        public TagSetPerFilter GetPresentOrMissing(bool isArticlePresent)
        {
            return isArticlePresent ? this.Present : this.Missing;
        }
    

        public override string ToString()
        {
            return this.Tag.Name;
        }
    }

    public class TagSetPerFilter
    {
        public readonly TagStatOnSubset Account = new TagStatOnSubset();
        public readonly TagStatOnSubset GeneralOnKnownArticles = new TagStatOnSubset();
        public readonly TagStatOnSubset GeneralOnAllArticles = new TagStatOnSubset();


        public void CleanForAccount()
        {
            this.Account.Clean();
            this.GeneralOnKnownArticles.Clean();
           ///except: this.GeneralOnAllArticles.Clean();
        }
        public void Refresh()
        {
            this.Account.Refresh();
            this.GeneralOnKnownArticles.Refresh();
            this.GeneralOnAllArticles.Refresh();
        }          
    }

    public class TagStatOnSubset
    {
        public int Like;
        public int Visited;
        public int Disliked;

        public int Total;
        

        public void Clean()
        {
            this.Like = 0;
            this.Visited = 0;
            this.Disliked = 0;
            this.Total = 0;
          

        }
        public void Refresh()
        {
            this.Total = this.Like + this.Visited + this.Disliked;           
        }

        public int GetPredictedScore()
        {
            if (this.Like >= this.Visited && this.Like >= this.Disliked)
            {
                return 1;
            }
            else if (this.Visited >= this.Disliked)
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }
        public int GetClick(int category)
        {
            switch (category)
            {
                case 1: return this.Like;
                case 0: return this.Visited;
                case -1: return this.Disliked;
                default: throw new NotImplementedException("" + category);
            }
        }
        public double GetClickProba(int category)
        {
            return this.GetClick(category) * 1.0 / this.Total;
        }
    }

    public class TagStatWeight : IComparable<TagStatWeight>, IComparable
    {
        public readonly TagStat TagStat;
        public readonly int category;
        public readonly bool isArticlePresent;

        public readonly double ResultAccuracy;

        public readonly int CompAccuracy;///Lower is better
        public readonly int CompArticlePresent;
        public readonly int AccountTotal;
        public readonly int CompAccountTotal;
        public readonly int TagTotal;
        public readonly int CompTagTotal;

        public TagStatWeight(TagStat tagStat, int category, bool isArticlePresent)
        {
            this.TagStat = tagStat;
            this.category = category;
            this.isArticlePresent = isArticlePresent;


            TagSetPerFilter selected = tagStat.Present;
            TagSetPerFilter unselected = tagStat.Missing;
            if (!isArticlePresent)
            {
                selected = tagStat.Missing;
                unselected = tagStat.Present;
            }

            int nbClick = selected.Account.GetClick(category);
            if (selected.Account.Total != 0)
            {
                this.ResultAccuracy = nbClick * 1.0 / selected.Account.Total;
            }
            else
            {
                this.ResultAccuracy = 0;
            }

            this.CompAccuracy = LogForRank((1.0 - this.ResultAccuracy) * 100);
            this.CompArticlePresent = isArticlePresent ? 1 : 0;
            this.AccountTotal = selected.Account.Total;
            this.CompAccountTotal = LogForRank(this.AccountTotal);
            this.TagTotal = selected.GeneralOnAllArticles.Total;
            this.CompTagTotal = LogForRank(this.TagTotal);
        }


        public int CompareTo(TagStatWeight that)
        {
            int r;
            r = this.CompAccuracy.CompareTo(that.CompAccuracy);
            if (r != 0)
            {
                return -r;
            }
            r = this.CompArticlePresent.CompareTo(that.CompArticlePresent);
            if (r != 0)
            {
                return r;
            }
            r = this.CompAccountTotal.CompareTo(that.CompAccountTotal);
            if (r != 0)
            {
                return r;
            }
            r = this.CompTagTotal.CompareTo(that.CompTagTotal);
            if (r != 0)
            {
                return r;
            }
            return 0;///Idem
        }
        
        public int CompareTo(object other)
        {
            return this.CompareTo((TagStatWeight)other);
        }

        private static readonly int[] logRanges = new[] { 0, 1, 2, 3, 4, 5, 6, 8, 10, 15, 20, 25, 30, 40, 50, 75, 100, 200, 400, 800, 1600, 3200, 6400, 12800, 25600, 51200, 102400 };
            
        public static int LogForRank(double value)
        {
            if (Math.Abs(value) < double.Epsilon)
            {
                return 0;
            }
            if (value < 0)
            {
                return -1 * LogForRank(value * -1);
            }
            for (int i = 0; i < logRanges.Length; i++)
            {
                if (value < logRanges[i])
                {
                    return i;
                }
            }
            return logRanges.Length;
        }

        public static int RoundToLog(double value)
        {
            if (Math.Abs(value) < double.Epsilon)
            {
                return 0;
            }
            if (value < 0)
            {
                return -1 * LogForRank(value * -1);
            }
            for (int i = 0; i < logRanges.Length; i++)
            {
                if (value < logRanges[i])
                {
                    return logRanges[i];
                }
            }
            return logRanges[logRanges.Length - 1];
        }

        public override string ToString()
        {
            return (this.isArticlePresent ? "" : " (-)") + this.TagStat.Tag.Name + 
                     " " + this.ResultAccuracy.ToString("0.00");
        }
    }
}
