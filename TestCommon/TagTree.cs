﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    /// <summary>
    /// Tag ordonned by correlation
    /// </summary>
    public class TagTree
    {
        private Model ds;
        public List<UnrelatedGroup> Root = new List<UnrelatedGroup>();
        public List<Tag> Trash = new List<Tag>();

        public TagTree(Model ds)
        {
            this.ds = ds;

            foreach (Tag tag in ds.Tags.Values.OrderByDescending(t => t.ArticleTags.Count))
            {
                this.Add(tag);
            }


        }


        private static int GetTagCorrelation(HashSet<Article> tagArticles, Tag testedTag)
        {
            int nbCommonTag = testedTag.ArticleTags.Select(at => at.Article).Where(a => tagArticles.Contains(a)).Count();
            return nbCommonTag;
        }
        private static double GetTagInclusion(HashSet<Article> tagArticles, Tag testedTag)
        {
            int nbCommonTag = GetTagCorrelation(tagArticles, testedTag);
            return nbCommonTag * 1.0 / testedTag.ArticleTags.Count;
        }
        public static double GetTagInclusion(Tag rootTag, Tag testedTag)
        {
            HashSet<Article> rootArticles = new HashSet<Article>(rootTag.ArticleTags.Select(at => at.Article));
            double correlation = GetTagInclusion(rootArticles, testedTag);
            return correlation;
        }

        public void Add(Tag tag)
        {
            foreach (UnrelatedGroup children in this.Root)
            {
                if (children.TryAdd(tag))
                {
                    return;
                }
            }
            if (this.Root.Count < 50)
            {
                this.Root.Add(new UnrelatedGroup(tag));
                return;
            }
            this.Trash.Add(tag);
        }
    }


    public abstract class TagTreeNode
    {
        public Tag President;
        public List<Tag> Trash = new List<Tag>();
        public int InnnerTagsCount = 1;


        public abstract IEnumerable<TagTreeNode> GetChildren();
        protected abstract bool tryAdd(Tag tag);

        public bool TryAdd(Tag tag)
        {
            bool added = this.tryAdd(tag);
            if (added)
            {
                this.InnnerTagsCount++;
            }
            return added;
        }
        public string GetDebugDisplay(Model db)
        {
            return this.GetType().Name + ": " + this.President +
                Environment.NewLine +
                "Innner tags counts: " + this.InnnerTagsCount +
                Environment.NewLine +
                "All child's articles: " + this.getDistinctChildrenArticlesCount(db) +

                Environment.NewLine + Environment.NewLine +
                "Children: " + Environment.NewLine +
                 string.Join(Environment.NewLine, this.GetChildren().OrderByDescending(g => g.InnnerTagsCount).Select(c => "  " + c.getDistinctChildrenArticlesCount(db) + "   " + c.President.Name )) +
               Environment.NewLine + Environment.NewLine +
                 "Trash: " + Environment.NewLine +
                string.Join(Environment.NewLine, this.Trash.OrderByDescending(t => t.ArticleTags.Count).Select(t => "  " + t.ArticleTags.Count + "   " + t.Name));
        }

        private void getAllChildrenTags(HashSet<Tag> list)
        {
            list.Add(this.President);
            foreach (TagTreeNode child in this.GetChildren())
            {
                child.getAllChildrenTags(list);
            }
            list.UnionWith(this.Trash);
        }
        private int? distinctChildrenArticlesCount = null;
        private int getDistinctChildrenArticlesCount(Model db)
        {
            if (this.distinctChildrenArticlesCount != null)
            {
                return distinctChildrenArticlesCount.Value;///Already computed
            }
            HashSet<Tag> tagList = new HashSet<Tag>();
            this.getAllChildrenTags(tagList);
            this.distinctChildrenArticlesCount = db.Articles.Values.Where(a => a.ArticleTags.Any(at => tagList.Contains(at.Tag))).Count();
            return this.distinctChildrenArticlesCount.Value;
        }
    }

    public class UnrelatedGroup : TagTreeNode
    {
        
        public List<OpposedGroup> Children = new List<OpposedGroup>();
       
        public UnrelatedGroup(Tag president)
        {
            this.President = president;
            this.Children.Add(new OpposedGroup(president));
        }
        public override string ToString()
        {
            if (this.Children.Count < 2)
            {
                return "MonoUnrelatedGroup <" + this.President + ">";
            }
            return "Dimensions: " + string.Join(", ", this.Children.Take(5).Select(g => "<" + g.President.Name + ">").ToArray());

        }

        public override IEnumerable<TagTreeNode> GetChildren()
        {
            return this.Children.AsEnumerable<TagTreeNode>();
        }
        protected override bool tryAdd(Tag tag)
        {
            foreach (OpposedGroup children in this.Children)
            {
                if (children.TryAdd(tag))
                {
                    return true;
                }
            }
            foreach (OpposedGroup child in this.Children)
            {
                double correlation = TagTree.GetTagInclusion(child.President, tag);
                if (correlation < 0.30 || correlation > 0.70)
                {
                    return false;
                }
            }
            if (this.Children.Count < 100)
            {
                this.Children.Add(new OpposedGroup(tag));
                return true;
            }
            this.Trash.Add(tag);
            return true;
        }
    }

    public class OpposedGroup : TagTreeNode
    {
        public List<RelatedGroup> Children = new List<RelatedGroup>();

        public OpposedGroup(Tag president)
        {
            this.President = president;
            this.Children.Add(new RelatedGroup(this.President));
        }

        public override string ToString()
        {
            if (this.Children.Count < 2)
            {
                return "MonoOpposedGroup <" + this.President.Name + ">";
            }
            return "Choose: <" + this.Children[0].President.Name + "> vs <" + this.Children[1].President.Name + ">";
        }
        protected override bool tryAdd(Tag tag)
        {
            foreach (RelatedGroup children in this.Children)
            {
                if (children.TryAdd(tag))
                {
                    return true;
                }
            }
            if (tag.ArticleTags.Count + 1 < this.President.ArticleTags.Count / 5)
            {
                return false;///Keep proportion
            }
            foreach (RelatedGroup child in this.Children)
            {
                if (TagTree.GetTagInclusion(child.President, tag) > 0.01)
                {
                    return false;
                }
            }
            if (this.Children.Count < 50)
            {
                this.Children.Add(new RelatedGroup(tag));
                return true;
            }
            this.Trash.Add(tag);
            return true;
        }

        public override IEnumerable<TagTreeNode> GetChildren()
        {
            return this.Children.AsEnumerable<TagTreeNode>();
        }

    }
    public class RelatedGroup : TagTreeNode
    {
        public List<UnrelatedGroup> Children = new List<UnrelatedGroup>();
        
        public RelatedGroup(Tag president)
        {
            this.President = president;

        }
        public override string ToString()
        {
            if (this.Children.Count == 0)
            {
                return "<" + this.President.Name + ">";
            }
            return "<" + this.President.Name + "> and specialities";
        }

        public override IEnumerable<TagTreeNode> GetChildren()
        {
            return this.Children.AsEnumerable<TagTreeNode>();
        }       

        protected override bool tryAdd(Tag tag)
        {
            if (TagTree.GetTagInclusion(this.President, tag) < 0.90)
            {
                return false;
            }
            foreach (UnrelatedGroup children in this.Children)
            {
                if (children.TryAdd(tag))
                {
                    return true;
                }
            }
            if (this.Children.Count < 100)
            {
                this.Children.Add(new UnrelatedGroup(tag));
                return true;
            }
            this.Trash.Add(tag);
            return true;
        }
    }




}
