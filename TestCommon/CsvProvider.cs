﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
	public static class CsvProvider
	{

		private static string csvSanitize(string value)
		{
			return value.Replace(",", "").Replace(";", "").Replace("\t", "").Replace("\r", "").Replace("\n", "").Replace("|", "");
		}
		public static void SaveInCsv(string fileName, IEnumerable<Article> articles)
		{
			if (File.Exists(fileName))
			{
				File.Delete(fileName);
			}
			using (var file = new StreamWriter(fileName))
			{
				file.WriteLine("Title,Url,NumberOfView,Rating,Tags");
				foreach (Article article in articles) 
				{					
					string[] tags = article.ArticleTags.Select(t => t.Tag.Name).Select(v => csvSanitize(v)).ToArray();
					file.WriteLine(csvSanitize(article.Title)
						+ "," + csvSanitize(article.Url)
						+ "," + csvSanitize(article.NumberOfView.ToString())
						+ "," + csvSanitize(article.Rating.ToString())
						+ "," + string.Join("|", tags)
						);

				}
			}
		}

		public static void LoadArticlesFromCsv(string fileName, Model db)
		{
			using (var file = File.OpenText(fileName))
			{

				string currentLine;
				currentLine = file.ReadLine();
				if (currentLine != "Title,Url,NumberOfView,Rating,Tags")
				{
					throw new Exception();
				}
				while ((currentLine = file.ReadLine()) != null)
				{
					string[] split = currentLine.Split(',');
					string url = split[1];

					if (!db.Articles.ContainsKey(url))
					{
						db.Articles.Add(url, new Article { Url = url });
					}
					Article article = db.Articles[url];
					article.Title = split[0];
					article.NumberOfView = int.Parse(split[2]);
					article.Rating = int.Parse(split[3]);
					string tagCsv = split[4];
					string[] splitTag = tagCsv.Split('|');
					foreach (string tagName in splitTag)
					{

						if (!db.Tags.ContainsKey(tagName))
						{
							db.Tags.Add(tagName, new Tag
							{
								Name = tagName
							});
						}
						Tag tag = db.Tags[tagName];
						if (!article.ArticleTags.Any(at => at.Tag == tag))
						{
							article.ArticleTags.Add(new ArticleTag { Tag = tag });
						}
					}
				}
			}
		}


	}
}
