﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
	public class SumProductPrediction
	{
		private Model db;
		private Account account;
		private static Random rand = new Random();
		private Dictionary<Tag, double> tagUsage = new Dictionary<Tag, double>();


		public SumProductPrediction(Model db, Account account)
		{
			this.db = db;
			this.account = account;
			foreach (Tag tag in db.Tags.Values)
			{
				double score = 0;
				foreach (Article article in tag.ArticleTags.Select(at => at.Article))
				{
					score += (article.NumberOfView * article.Rating)??0;
				}
				this.tagUsage[tag] = score;
			}
		}

		#region ImprovePrediction
		/// <summary>
		/// Return a note between -1 and 1 (with exception out of range)
		/// </summary>
		private double NoteAgainstObservedOpinion(AccountPredictor accountPredictor, Opinion opinion)
		{
			double predictionScore = accountPredictor.PredictScore(opinion.Article);

			if (opinion.Liked)///Target 1.00
			{
				if (predictionScore > 50.00)
				{
					return 0.90;///Too too big
				}				
				if (predictionScore > 0.75)
				{
					return 5;///Perfect
				}
				if (predictionScore > 0.50)
				{
					return 4;
				}
				if (predictionScore > 0.30)
				{
					return 0.3;
				}
				if (predictionScore > 0.10)
				{
					return -0.1;
				}
                if (predictionScore > -0.20)
                {
                    return -0.5;
                }
				return -1;///All wrong!

			}
			if (opinion.Disliked)///Target -1.00
			{
				if (predictionScore > 0.50)
				{
					return -2;///All wrong
				}				
				if (predictionScore > -0.50)
				{
					return -1;
				}                
				if (predictionScore > -50)
				{
					return 5;///Good
				}				
				return 0.9;
			}
			if (opinion.Visited)
			{
				if (predictionScore > 1)
				{
					return -0.5;///Over enthousiaste
				}
				if (predictionScore > 0.50)
				{
					return -0.25;///Over enthousiaste
				}
				if (predictionScore > -0.50)
				{
					return 1;///Good
				}
				if (predictionScore > -1)
				{
					return -0.25;
				}
				return -0.5;
			}
			return 0;
		}
    

		public double NoteBatch(AccountPredictor accountPredictor)
		{
			double totalNotes = 0;
			foreach (Opinion opinion in account.Opinions)
			{
              	double note = NoteAgainstObservedOpinion(accountPredictor, opinion);
				totalNotes += note;
			}
			return (totalNotes / account.Opinions.Count);//; * (10.0 / (accountPredictor.ClauseList.Where(c => c.Coeficient != 0).Count() + 1));
		}


		public string DebugGetReport()
		{
			AccountPredictor accountPredictor = new AccountPredictor(this.db, this.account);
			double noteBatch = this.NoteBatch(accountPredictor);
			string[] accountParam = accountPredictor.DebugString();
			return "Accuracy: " + noteBatch.ToString("0.00") + Environment.NewLine + "Clause coefs:" + Environment.NewLine + string.Join(Environment.NewLine, accountParam);
		}
		
		public void ImprovePrediction()
		{
			AccountPredictor accountPredictor = new AccountPredictor(this.db, this.account);
			this.addARandomClause(accountPredictor);
			double bestNote = NoteBatch(accountPredictor);
			foreach (ClausePredictor clause in accountPredictor.ClauseList)
			{				
				for (int i = 0; i < 3; i++)
				{					
					double newValue = clause.Coeficient + rand.NextDouble() - 0.5;
					tryImproveClause(accountPredictor, clause, ref bestNote, newValue);
				}
			}
			///Try the zero value to check if the clause is useless.
			foreach (ClausePredictor clause in accountPredictor.ClauseList)
			{				
				tryImproveClause(accountPredictor, clause, ref bestNote, 0);
			}
			accountPredictor.Save();
		}

		private void addARandomClause(AccountPredictor accountPredictor)
		{
			List<Tag> allViewed = this.account.Opinions
				.SelectMany(o => o.Article.ArticleTags)
				.Select(at => at.Tag)
				.Distinct()
				.OrderByDescending(tag => this.tagUsage[tag])
				.Take(this.account.Opinions.Count() * 5)
				.ToList();
			if (allViewed.Count == 0)
			{
				return;
			}
			foreach (Tag tag in allViewed)
			{				
				foreach (PreditorTest test in new[] { PreditorTest.TagPresent, PreditorTest.TagMissing })
				{
					ClausePredictor newClause = new ClausePredictor
					{
						Tag = allViewed[rand.Next(allViewed.Count)],
						Test = test,
						Coeficient = 0
					};
					if (!accountPredictor.ClauseList.Any(c => c.Tag == newClause.Tag && c.Test == newClause.Test))
					{
						accountPredictor.ClauseList.Add(newClause);
					}
				}
			}
		}

		

		private void tryImproveClause(AccountPredictor accountPredictor, ClausePredictor clause, ref double bestNote, double v)
		{
			double previous = clause.Coeficient;
			clause.Coeficient = v;
			double note = NoteBatch(accountPredictor);
			if (note >= bestNote)
			{
				bestNote = note;
			}
			else
			{
				clause.Coeficient = previous;///Rollback
			}			
		}

		#endregion ImprovePrediction

		#region GetNextArticles

		public List<Article> GetNextArticles(bool usePrediction)
		{
			List<Article> returned = new List<Article>();
			HashSet<Article> alreadyView = new HashSet<Article>(this.account.Opinions.Select(o => o.Article));
            if (usePrediction)
            {
                double minScore = 0.99;
                for (int i = 0; i < 6; i++)
                {
                    tryAddBestArticle(returned, alreadyView, ref minScore);
                }
            }
			for (int i = returned.Count; i < 7; i++)
			{
				Article article = GetPivotArticle(alreadyView, 10 - i);
				if (article != null)
				{
					returned.Add(article);
					alreadyView.Add(article);
				}
				else
				{
					throw new NotImplementedException();
				}
			}
			return returned;
		}

		private void tryAddBestArticle(List<Article> returned, HashSet<Article> alreadyView, ref double minScore)
		{
			Article article = GetBestArticle(alreadyView, minScore);
			if (article != null)
			{
				returned.Add(article);
				alreadyView.Add(article);
			}
			else
			{
				minScore -= 0.10;
			}
		}

		private Article GetBestArticle(HashSet<Article> alreadyView, double minScore)
		{			
			AccountPredictor accountPredictor = new AccountPredictor(this.db, this.account);
			double best = minScore;
			Article bestArticle = null;
			foreach (Article article in this.db.Articles.Values)
			{
				if (alreadyView.Contains(article))
				{
					continue;
				}
				double articleScore = accountPredictor.PredictScore(article);
				if(articleScore < minScore)
				{
					continue;
				}
				double score = ComputeArticlePivotLevel(article);
				if (score > best)
				{
					best = score;
					bestArticle = article;
				}
			}			
			return bestArticle;
		}
	
		/// <summary>
		/// Probably the user will not like, but give a try
		/// </summary>
        private Article GetPivotArticle(HashSet<Article> alreadyView, int maxRating)
		{
			double best = double.MinValue;
			Article bestArticle = null;


			foreach (Article article in this.db.Articles.Values)
			{
				if (alreadyView.Contains(article))
				{
					continue;
				}
                double score = ComputeArticlePivotLevel(article, maxRating);
				if (score > best)
				{
					best = score;
					bestArticle = article;
				}
			}
			return bestArticle;
		}

        private double ComputeArticlePivotLevel(Article article, int maxRating=10)
		{
			if( article.ArticleTags.Count == 0)
			{
				return -1;
			}
            int mediumTagCount = this.db.Tags.Count / (this.db.Articles.Count + 1);
            double coef = article.ArticleTags.Count >= mediumTagCount ? 1 : mediumTagCount - article.ArticleTags.Count;
			//return (1 + article.NumberOfView ?? 0) * (1 + article.Rating ?? 0) / article.ArticleTags.Count;

            if (article.Rating > maxRating)
            {
                double diff = (article.Rating - maxRating);
                coef = coef / (2.0 + diff * diff);
            }

            return (1 + article.NumberOfView ?? 0) * coef;
		}

		#endregion GetNextArticles
	}
}
