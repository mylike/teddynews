﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
	/// <summary>
	/// Database in memory
	/// </summary>
	public class Model
	{

        

		public Dictionary<string, Article> Articles = new Dictionary<string, Article>();
		public Dictionary<string, Tag> Tags = new Dictionary<string, Tag>();
		public Dictionary<int, Account> Accounts = new Dictionary<int, Account>();


		private Dictionary<Article, HashSet<Tag>> articleTagIndex = new Dictionary<Article, HashSet<Tag>>();
		public bool ArticleTagExist(Article article, Tag tag)
		{
			HashSet<Tag> tagSet;
			if (!this.articleTagIndex.TryGetValue(article, out tagSet))
			{
				tagSet = new HashSet<Tag>(article.ArticleTags.Select(at => at.Tag));
				this.articleTagIndex[article] = tagSet;
			}
			return tagSet.Contains(tag);
		}

		public void TryAddTag(Article article, string tagName)
		{
			if (!this.Tags.ContainsKey(tagName))
			{
				this.Tags[tagName] = new Tag{Name=tagName};
			}
			Tag tag = this.Tags[tagName];
			if (!article.ArticleTags.Any(at => at.Tag == tag))
			{
				article.ArticleTags.Add(new ArticleTag { Tag = tag });
				this.articleTagIndex.Clear();
			}
		}
       
	}


   
}
