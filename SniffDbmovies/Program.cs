﻿using Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoadDbmovies
{

	/// <summary>
	/// Parse the IMDb database
	/// 
	/// IMDb files can be found theere: http://www.imdb.com/interfaces
	/// </summary>
	class Program
	{
		private const string dir = @"C:\Temp\";

		static void Main(string[] args)
		{
			Dictionary<string, Article> movies = ParseRatings();
			Trace.WriteLine("Number of films: " + movies.Count);
			Dictionary<string, Tag> tags = new Dictionary<string,Tag>();
			ParseKeyword("genres.list", movies, tags);
			ParseKeyword("keywords.list", movies, tags);
			Trace.WriteLine("Keyword parsed");
			Dictionary<string, Article> savedMovies = movies
				.Where(m => !m.Value.Title.StartsWith("#") && m.Value.ArticleTags.Count > 0)
				.ToDictionary(m => m.Key, m=> m.Value);

			ParseActor("actors.list", savedMovies, tags);
			ParseActor("actresses.list", savedMovies, tags);
			ParseActor("producers.list", savedMovies, tags);
			ParseActor("directors.list", savedMovies, tags);
			
			CsvProvider.SaveInCsv(Path.Combine(dir, "MoviesFull.csv"), savedMovies.Values);
			CsvProvider.SaveInCsv(Path.Combine(dir, "Movies.csv"), savedMovies.Values.OrderByDescending(m => m.NumberOfView).Take(200));

			Trace.WriteLine("File saved");
		}

		private static Dictionary<string, Article> ParseRatings()
		{			
			Dictionary<string, Article> movies = new Dictionary<string, Article>();
			using (var file = File.OpenText(Path.Combine(dir, "ratings.list")))
			{
				Trace.WriteLine("File opened");
				bool ratingStarted = false;
				string currentLine;
				while ((currentLine = file.ReadLine()) != null)
				{
					if (!ratingStarted)
					{
						if (currentLine == "MOVIE RATINGS REPORT")
						{
							ratingStarted = true;
							Trace.WriteLine("rating start");
						}
						continue;
					}

					if (currentLine.StartsWith("-----------------------------"))
					{
						break;///End of the list
					}

					///Format:
					///New  Distribution  Votes  Rank  Title
					///      2000000102      60   6.2  "#1 Single" (2006)
					///      0..0000124      68   8.2  "Standard Action" (2010)      
					int voteCol = 17;
					int rankCol = 26;
					int titleCol = 32;
					if (currentLine.Length < titleCol + 10)
					{
						continue;
					}
					string voteString = currentLine.Substring(voteCol, (rankCol - voteCol)).Trim();
					string rankString = currentLine.Substring(rankCol, (titleCol - rankCol)).Trim();
					string titleString = currentLine.Substring(titleCol).Trim();
					///Mr. A (2008)
					if(titleString.StartsWith("\""))
					{
						continue;
					}
					if(!titleString.EndsWith(")"))
					{
						continue;
					}
					if(titleString.Where(c => c=='(').Count() != 1)
					{
						continue;
					}
					movies[titleString] = new Article
					{
						Url = titleString,
						Title = titleString,
						Rating = (int) Math.Round(double.Parse(rankString), 0),
						NumberOfView = int.Parse(voteString)
					};					
				}				
			}
			return movies;
		}

		private static void ParseKeyword(string fileName, Dictionary<string, Article> articles, Dictionary<string, Tag> tags)
		{
			using (var file = File.OpenText(Path.Combine(dir, fileName)))
			{
				Trace.WriteLine("File opened");
				string currentLine;
				while ((currentLine = file.ReadLine()) != null)
				{

					if (currentLine == "8: THE KEYWORDS LIST")
					{
						Trace.WriteLine("found: " + currentLine);
						break;

					}
					if (currentLine == "8: THE GENRES LIST")
					{
						Trace.WriteLine("found: " + currentLine);
						break;
					}
				}
				while ((currentLine = file.ReadLine()) != null)
				{
					string[] split = currentLine.Split(new[] { "\t" }, StringSplitOptions.RemoveEmptyEntries);
					if (split.Length != 2)
					{
						continue;
					}
					string movie = split[0].Trim();
					string tag = split[1].Trim();
					if (!articles.ContainsKey(movie))
					{
						continue;
					}
					if (!tags.ContainsKey(tag))
					{
						tags.Add(tag, new Tag
						{
							Name = tag
						});
					}
					Article article = articles[movie];
					article.ArticleTags.Add(new ArticleTag { Tag = tags[tag] });
				}
			}
		}

		private static void ParseActor(string fileName, Dictionary<string, Article> articles, Dictionary<string, Tag> tags)
		{
			using (var file = File.OpenText(Path.Combine(dir, fileName)))
			{
				Trace.WriteLine(fileName + " file opened");
				string currentLine; 
				while ((currentLine = file.ReadLine()) != null)
				{
					if (currentLine == "THE ACTORS LIST")
					{
						break;
					}
					if (currentLine == "THE ACTRESSES LIST")
					{
						break;
					}
					if (currentLine == "THE PRODUCERS LIST")
					{
						break;
					}
					if (currentLine == "THE DIRECTORS LIST")
					{
						break;
					}
					
				}
				while (!file.ReadLine().StartsWith("---"))
				{
				}				
				string currentActor = null;				
				while ((currentLine = file.ReadLine()) != null)
				{
					if (string.IsNullOrWhiteSpace(currentLine))
					{
						continue;
					}
					if (currentLine.StartsWith("-----"))
					{
						break;
					}
					string movieName;
					if (currentLine.StartsWith("\t"))
					{
						movieName = currentLine.Trim();
					}
					else
					{
						string[] split = currentLine.Split(new char[] {'\t' }, StringSplitOptions.RemoveEmptyEntries);
						currentActor = split[0];
						movieName = split[1];
					}
					int braketIndex = movieName.IndexOf("[");
					if (braketIndex > 3)
					{
						movieName = movieName.Substring(0, braketIndex - 2);
					}

					if (!articles.ContainsKey(movieName))
					{
						continue;
					}
					if (!tags.ContainsKey(currentActor))
					{
						tags.Add(currentActor, new Tag
						{
							Name = currentActor
						});
					}
					Article article = articles[movieName];
					article.ArticleTags.Add(new ArticleTag { Tag = tags[currentActor] });
				}
			}
		}

	}
}
