﻿using Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string sqlFileName = Path.GetFullPath(@"..\..\..\Common\Database.sql");
                if (!File.Exists(sqlFileName))
                {
                    throw new ArgumentException(sqlFileName);
                }

                using (SqlConnection con = new SqlConnection(@"Data Source=.;Initial Catalog=MyLike;Integrated Security=True"))
                {
                    con.Open();
                    Console.WriteLine("DB openned");
                    string fileContent = File.ReadAllText(sqlFileName);
                    foreach (string query in fileContent.Split(new[] { "GO"}, StringSplitOptions.RemoveEmptyEntries))
                    {
                        if (string.IsNullOrWhiteSpace(query))
                        {
                            continue;
                        }

                        using (var command = con.CreateCommand())
                        {
                            command.CommandText = query;
                            command.ExecuteNonQuery();
                        }

                    }
                    Console.WriteLine("DB created");                   
                }


                string fileNameFull = @"C:\temp\MoviesFull.csv";
                string fileNameShort = @"C:\temp\MoviesShort.csv";
                if (!File.Exists(fileNameShort))
                {
                    if (!File.Exists(fileNameFull))
                    {
                        throw new FileNotFoundException(fileNameShort);
                    }
                    Console.WriteLine("create short model");
                    Model fullModel = new Model();
                    CsvProvider.LoadArticlesFromCsv(fileNameFull, fullModel);
                    CsvProvider.SaveInCsv(fileNameShort, fullModel.Articles.Values.OrderByDescending(a => a.NumberOfView).Take(200));

                }
                Console.WriteLine("Load model");
                Model shortModel = new Model();
                CsvProvider.LoadArticlesFromCsv(fileNameShort, shortModel);
                ///Set dates
                foreach (Article article in shortModel.Articles.Values)
                {
                    article.Url = "http://www.google.com/search?q=" + article.Title;
                    article.DateTimeStamp = new DateTime(2015, 1, 1);
                    string name = article.Title;
                    if (name.Contains("("))
                    {
                        string yearString = name.Substring(name.IndexOf("(") + 1);
                        yearString = yearString.Substring(0, yearString.Length - 1);
                        int year;
                        if (int.TryParse(yearString, out year))
                        {
                            article.DateTimeStamp = new DateTime(year, 1, 1);
                        }
                    }
                }
                Console.WriteLine("Fill DB...");
                MyLikeEntities db = new MyLikeEntities();
                foreach (Article article in shortModel.Articles.Values)
                {
                    
                    db.Articles.Add(article);
                }
                db.SaveChanges();
                Console.WriteLine("Ok");
            }
            catch (Exception ex) 
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex);
            }
            Console.ResetColor();
            Console.WriteLine("Press enter to exit...");
            Console.ReadLine();
        }
    }
}
