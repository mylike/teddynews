﻿using Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyLikeDemo
{
    public partial class Form1 : Form
    {

        private DataTable showedArticles;
        //private bool isUserAction = true;
        private int currentTime = 2000;
        private DateTime now = new DateTime(2000, 6, 1);

        private Model db = new Model();
        private const int UserAccountId = 123;
        private Account Account;
       
        public Form1()
        {
            InitializeComponent();
        }

        public void Init()
        {
            string fileNameFull = @"C:\temp\MoviesFull.csv";
            string fileNameShort = @"C:\temp\MoviesShort.csv";
            if (!File.Exists(fileNameShort))
            {
                if (!File.Exists(fileNameFull))
                {
                    throw new FileNotFoundException(fileNameShort);
                }
                Trace.WriteLine("create short model");
                Model fullModel = new Model();
                CsvProvider.LoadArticlesFromCsv(fileNameFull, fullModel);
                CsvProvider.SaveInCsv(fileNameShort, fullModel.Articles.Values.OrderByDescending(a => a.NumberOfView).Take(2000));

            }
            Trace.WriteLine("Load model");
            CsvProvider.LoadArticlesFromCsv(fileNameShort, this.db);            
            Trace.WriteLine("Model loaded");
            ///Set dates
            foreach (Article article in db.Articles.Values)
            {
				article.DateTimeStamp = new DateTime(2015, 1, 1);
                string name = article.Title;
                if (name.Contains("("))
                {
                    string yearString = name.Substring(name.IndexOf("(") + 1);
                    yearString = yearString.Substring(0, yearString.Length - 1);
                    int year;
                    if (int.TryParse(yearString, out year))
                    {
						article.DateTimeStamp = new DateTime(year, 1, 1);
                    }
                }
            }
           
            this.Account = new Account { Id = UserAccountId };
            this.db.Accounts.Add(this.Account.Id, this.Account);

            
            this.showedArticles = new DataTable();
            this.showedArticles.Columns.Add("Title");
            this.showedArticles.Columns.Add("Like", typeof(bool)).AllowDBNull = false;
            this.showedArticles.Columns.Add("Visited", typeof(bool)).AllowDBNull = false;
            this.showedArticles.Columns.Add("Dislike", typeof(bool)).AllowDBNull = false;

            
            this.showedArticles.DefaultView.AllowNew = false;
            this.dataGridView.DataSource = this.showedArticles;
            this.dataGridView.Columns["Title"].Width = 400;
            this.dataGridView.Columns["Like"].Width = 48;
            this.dataGridView.Columns["Visited"].Width = 48;
            this.dataGridView.Columns["Dislike"].Width = 48;
            this.dataGridView.Columns["Like"].HeaderText = "Like";
            this.dataGridView.Columns["Visited"].HeaderText = "Visited";
            this.dataGridView.Columns["Dislike"].HeaderText = "Dislike";
            this.dataGridView.Columns["Title"].ReadOnly = true;

            showMoreResults();
        }

        private void showMoreResults()
        {
            this.showedArticles.Rows.Clear();
            List<Article> newArticles = GreedyReco.GetMoreArticles(this.db.Articles.Values, this.Account, now);
            foreach (Article article in newArticles)
            {
                DataRow row = this.showedArticles.NewRow();
                row["Title"] = article.Title;
                row["Like"] = false;
                row["Visited"] = false;
                row["Dislike"] = false;
                this.showedArticles.Rows.Add(row);
                article.Opinions.Add(new Opinion { Account = this.Account });
            }
        }
        private void updateModelFromShowedArticles()
        {
            foreach (DataRow row in this.showedArticles.Select())
            {
                Article article = this.db.Articles[row["Title"].ToString()];
                Opinion opinion = article.Opinions.First(o => o.Account == this.Account);
                opinion.Liked = true.Equals(row["Like"]);
                opinion.Visited = true.Equals(row["Visited"]);
                opinion.Disliked = true.Equals(row["Dislike"]);
                GreedyReco.OpinionUpdated(opinion, now);
            }
        }
        private void moreButton_Click(object sender, EventArgs e)
        {
            updateModelFromShowedArticles();
            showMoreResults();
        }
        private void timeButton_Click(object sender, EventArgs e)
        {
            this.currentTime++;
            this.timeButton.Text = this.currentTime.ToString();
            this.now = new DateTime(this.currentTime, 6, 1);
            
        }

        
        
    }
}
