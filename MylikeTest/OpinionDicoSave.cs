﻿using Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Mylike
{
    /// <summary>
    /// Avoid tape on each try
    /// </summary>
    public class OpinionDicoSave
    {
        private string fileName;
        private Dictionary<Article, int> articleCategoryDico = new Dictionary<Article, int>();

        public OpinionDicoSave(Model db, string fileName)
        {
            this.fileName = fileName;
            if (File.Exists(this.fileName))
            {
                this.LoadCsv(db, this.fileName);
            }
        }
        public void Set(Article key, bool like, bool visited, bool dislike)
        {
            if (like)
            {
                this.articleCategoryDico[key] = 1;
            }
            else if (dislike)
            {
                this.articleCategoryDico[key] = -1;
            }
            else if (visited)
            {
                this.articleCategoryDico[key] = 0;
            }
            else
            {
                this.articleCategoryDico.Remove(key);
            }
        }

        public int Get(Article key)
        {
            if (this.articleCategoryDico.ContainsKey(key))
            {
                return this.articleCategoryDico[key];
            }
            return 777;
        }

        public void Save()
        {
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
            using (var file = new StreamWriter(fileName))
            {
                file.WriteLine("Title,AccountRating");
                foreach (Article article in this.articleCategoryDico.Keys.OrderBy(a => a.Title))
                {
                    file.WriteLine(article.Title + "," + this.articleCategoryDico[article]);
                }
            }
        }

        private void LoadCsv(Model db, string fileName)
        {            
            this.articleCategoryDico.Clear();
            using (var file = File.OpenText(fileName))
            {

                string currentLine;
                currentLine = file.ReadLine();
                if (currentLine != "Title,AccountRating")
                {
                    throw new Exception();
                }
                while ((currentLine = file.ReadLine()) != null)
                {                    
                    if(string.IsNullOrEmpty(currentLine))
                    {
                        continue;
                    }
                    string[] split = currentLine.Split(','); 
                   	string articleName = split[0];
					if (!db.Articles.ContainsKey(articleName))
					{
						continue;
					}
                    this.articleCategoryDico[db.Articles[articleName]] = int.Parse(split[1]);
                }
            }
        }
    }
}
