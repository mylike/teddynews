﻿using Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mylike
{
	public partial class TestForm : Form
	{
		private Model db = new Model();		
		private DataTable showedArticles;
		private bool isUserAction = true;

		private const int UserAccountId = 123;
		private Account Account;
		private SumProductPrediction sumProductPrediction;
        private TagReportPrediction tagReportPrediction;

        private OpinionDicoSave opinionDicoSave;

		public TestForm()
		{
			InitializeComponent();
		}

		public void Init()
		{
			string fileNameFull = @"C:\temp\MoviesFull.csv";
			string fileNameShort = @"C:\temp\MoviesShort.csv";
			string fileNameOpinionDico = @"C:\temp\opinionDico.csv";
			if (!File.Exists(fileNameShort))
			{
				if (!File.Exists(fileNameFull))
				{
					throw new FileNotFoundException(fileNameShort);
				}
				Trace.WriteLine("create short model");
				Model fullModel = new Model();
				CsvProvider.LoadArticlesFromCsv(fileNameFull, fullModel);
				CsvProvider.SaveInCsv(fileNameShort, fullModel.Articles.Values.OrderByDescending(a => a.NumberOfView).Take(200));

			}
			Trace.WriteLine("Load model");
			CsvProvider.LoadArticlesFromCsv(fileNameShort, this.db);

			this.opinionDicoSave = new OpinionDicoSave(this.db, fileNameOpinionDico);
			
            Trace.WriteLine("Model loaded");

			AddMovieTags(db);

			this.Account = new Account { Id = UserAccountId };
			this.db.Accounts.Add(this.Account.Id, this.Account);

			this.sumProductPrediction = new SumProductPrediction(this.db, this.Account);
            this.tagReportPrediction = new TagReportPrediction(this.db, this.Account);

			this.showedArticles = new DataTable();
			this.showedArticles.Columns.Add("Title");
			this.showedArticles.Columns.Add("Like", typeof(bool)).AllowDBNull = false;
			this.showedArticles.Columns.Add("Visited", typeof(bool)).AllowDBNull = false;
			this.showedArticles.Columns.Add("Dislike", typeof(bool)).AllowDBNull = false;
            
			this.showedArticles.ColumnChanged += showedArticles_ColumnChanged;

			this.showedArticles.DefaultView.AllowNew = false;
			this.dataGridView.DataSource = this.showedArticles;
			this.dataGridView.Columns["Title"].Width = 464;
			this.dataGridView.Columns["Like"].Width = 48;
			this.dataGridView.Columns["Visited"].Width = 48;
			this.dataGridView.Columns["Dislike"].Width = 48;
            this.dataGridView.Columns["Like"].HeaderText = "Must see";
            this.dataGridView.Columns["Visited"].HeaderText = "Can be";
            this.dataGridView.Columns["Dislike"].HeaderText = "Cant not";
			this.dataGridView.Columns["Title"].ReadOnly = true;
			
			this.showNextArticles();

			initArticleGraph();
			initTagGraph();
			initTagTree();
		}

	

		public static void AddMovieTags(Model model)
		{
			foreach (Article article in model.Articles.Values)
			{
				string name = article.Title;
				if (name.Contains("("))
				{
					string yearString = name.Substring(name.IndexOf("(")+1);
					yearString = yearString.Substring(0, yearString.Length -1);
					int year;
					if (int.TryParse(yearString, out year))
					{
						int roundYear = (year / 5) * 5;
						//model.TryAddTag(article, "years " + (roundYear - 5));
						model.TryAddTag(article, "years " + roundYear);
						//model.TryAddTag(article, "years " + (roundYear + 5));
					}
				}
				int rating = Math.Min(10, article.Rating);
				model.TryAddTag(article, "Rating " + rating);
                model.TryAddTag(article, "Rated by " + TagStatWeight.RoundToLog(article.NumberOfView ?? 0));				
			}
		}

		#region Tag Like
		private void showedArticles_ColumnChanged(object sender, DataColumnChangeEventArgs e)
		{
			if (!this.isUserAction)
			{
				return;
			}
			this.isUserAction = false;
			try
			{
				if (true.Equals(e.ProposedValue))
				{
					foreach (string column in new[] { "Like", "Visited", "Dislike" })
					{
						if (e.Column.ColumnName != column)
						{
							e.Row[column] = false;
						}
					}					
				}
			}
			finally
			{
				this.isUserAction = true;
			}
		}

		

        private void ReportComputationDebug()
        {
            DataRow selectedRow = ((DataRowView)this.dataGridView.Rows[dataGridView.CurrentCell.RowIndex].DataBoundItem).Row;
            Article selectedArticle = this.db.Articles[selectedRow["Title"].ToString()];
                
            
           this.computationResultTextBox.Text = this.tagReportPrediction.DebugDisplay(selectedArticle);
            
            this.colorPrediction();
        }

		private void colorPrediction()
		{
			AccountPredictor accountPredictor = new AccountPredictor(this.db, this.Account);
			foreach (DataGridViewRow gridRow in this.dataGridView.Rows)
			{
				DataRow row = ((DataRowView)gridRow.DataBoundItem).Row;
				Article article = this.db.Articles[row["Title"].ToString()];

				double score = this.tagReportPrediction.PredictScore(article).category;
				Color color = Color.Blue;
				bool isChecked = true.Equals(row["Like"]) || true.Equals(row["Visited"]) || true.Equals(row["Dislike"]);
				gridRow.Cells["Title"].Style.ForeColor = isChecked ? Color.Black : Color.DarkRed;
				gridRow.Cells["Like"].Style.BackColor = score >= 0.50 ? color : Color.White;
				gridRow.Cells["Visited"].Style.BackColor = score >= -0.25 && score <= 0.50 ? color : Color.White;
				gridRow.Cells["Dislike"].Style.BackColor = score <= -0.25 ? color : Color.White;

			}
		}
		private void addArticlesInGrid(List<Article> nextMovies)
		{
			foreach (Article article in nextMovies)
			{
				DataRow row = this.showedArticles.NewRow();
				row["Title"] = article.Title;
				int savedScore = this.opinionDicoSave.Get(article);
				row["Like"] = savedScore == 1;
				row["Visited"] = savedScore == 0;
				row["Dislike"] = savedScore == -1;
				this.showedArticles.Rows.Add(row);
				article.Opinions.Add(new Opinion { Account = this.Account });
			}
			this.dataGridView.FirstDisplayedScrollingRowIndex = this.showedArticles.Rows.Count - 1;
			this.colorPrediction();
		}

		private void updateModelFromShowedArticles()
		{
			foreach (DataRow row in this.showedArticles.Select())
			{
				Article article = this.db.Articles[row["Title"].ToString()];
				Opinion opinion = article.Opinions.First(o => o.Account == this.Account);
				opinion.Liked = true.Equals(row["Like"]);
				opinion.Visited = true.Equals(row["Visited"]);
				opinion.Disliked = true.Equals(row["Dislike"]);
				this.opinionDicoSave.Set(article, opinion.Liked, opinion.Visited, opinion.Disliked);
			}
			this.opinionDicoSave.Save();
		}


		public List<Article> GetNextArticles()
		{
			HashSet<Article> alreadyView = new HashSet<Article>(this.Account.Opinions.Select(o => o.Article));
			List<Article> notSeenArticles = this.db.Articles.Values
				.Where(a => !alreadyView.Contains(a))
				.OrderByDescending(a => a.NumberOfView).ToList();
			List<Article> returned = new List<Article>();
			///Look for probably-liked article
			foreach (Article article in notSeenArticles)
			{
				int prediction = this.tagReportPrediction.PredictScore(article).category;
				if (prediction == 1)
				{
					returned.Add(article);
					alreadyView.Add(article);
					if (returned.Count >= 3)
					{
						break;						
					}
				}
			}
			///Add some random article
			foreach (Article article in notSeenArticles)
			{
				if (returned.Count >= 7)
				{
					break;
				}
				if (!alreadyView.Contains(article))
				{
					returned.Add(article);
				}
			}
			return returned;
		}


		private void nextButton_Click(object sender, EventArgs e)
		{
			this.updateModelFromShowedArticles();
			this.tagReportPrediction.ImprovePrediction();
			this.showNextArticles();
			this.ReportComputationDebug();
		}
		//private void nextAffineButton_Click(object sender, EventArgs e)
		//{
		//	this.updateModelFromShowedArticles();
		//	this.tagReportPrediction.ImprovePrediction();
		//	List<Article> nextMovies = this.tagReportPrediction.GetNextArticlesForRefine();
		//	addArticlesInGrid(nextMovies);
		//	this.ReportComputationDebug();
		//}

		private void showNextArticles()
		{
			List<Article> nextMovies = this.GetNextArticles();
			addArticlesInGrid(nextMovies);
		}


		// private bool displaySumProductComputation = true;
		//private void affineButton_Click(object sender, EventArgs e)
		//{
		//	this.displaySumProductComputation = true;
		//	this.updateModelFromShowedArticles();
		//	this.sumProductPrediction.ImprovePrediction();
		//	this.ReportComputationDebug();

		//}

		//private void computeButton_Click(object sender, EventArgs e)
		//{
		//	this.displaySumProductComputation = true;
		//	this.UserAccount.AccountTastes.Clear();
		//	this.updateModelFromShowedArticles();
		//	this.sumProductPrediction.ImprovePrediction();
		//	this.ReportComputationDebug();

		//}

		private void tagReportButton_Click(object sender, EventArgs e)
		{
			//this.displaySumProductComputation = false;
			this.updateModelFromShowedArticles();
			this.tagReportPrediction.ImprovePrediction();
			this.ReportComputationDebug();
		}

		private void ReportTagButton_Click(object sender, EventArgs e)
		{
			//this.displaySumProductComputation = false;
			this.ReportComputationDebug();
		}

		#endregion Tag Like

		#region Article graph

		private HashSet<Article> graphedArticles = new HashSet<Article>();
		private HashSet<Article> rootedGraphedArticles = new HashSet<Article>();
		private TreeNode showArticleGraph(Article article)
		{
			if (rootedGraphedArticles.Contains(article))
			{
				return null;
			}
			TreeNode node = this.treeViewArticleGraph.Nodes.Add(article.Title);
			graphedArticles.Add(article);
			rootedGraphedArticles.Add(article);
			HashSet<Tag> articleTags = new HashSet<Tag>(article.ArticleTags.Select(at => at.Tag));

			List<Article> siblingArticles = this.db.Articles.Values				
				.Where(a => !graphedArticles.Contains(a))
				.OrderByDescending(a => GetCorrelation(articleTags, a))
				.Take(5)
				.ToList();
			foreach (Article sibling in siblingArticles)
			{
				node.Nodes.Add(sibling.Title);
				graphedArticles.Add(sibling);
			}
			node.Expand();
			return node;
		}

		private static int GetCorrelation(HashSet<Tag> articleTags, Article testedArticle)
		{
			int nbCommonTag = testedArticle.ArticleTags.Select(at => at.Tag).Where(t => articleTags.Contains(t)).Count();
			return nbCommonTag;
		}

		private void treeView_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			TreeNode node = this.treeViewArticleGraph.SelectedNode;
			if (node.Parent != null)
			{
				Article article = this.db.Articles[node.Text];
				TreeNode newNode = showArticleGraph(article);
				if (newNode != null)
				{
					this.treeViewArticleGraph.SelectedNode = newNode;
				}
			}
		}

		private void initArticleGraph()
		{
			List<Article> top100 = this.db.Articles.Values
					.OrderByDescending(a => a.NumberOfView)
					.Take(100).ToList();
			HashSet<Tag> articleTags = new HashSet<Tag>();
			for (int i = 0; i < 7; i++)
			{
				Article article = top100
					.OrderBy(a => GetCorrelation(articleTags, a))
					.First();
				showArticleGraph(article);
				articleTags.UnionWith(article.ArticleTags.Select(at => at.Tag));
			}
		}

		private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
		{
			TreeNode node = this.treeViewArticleGraph.SelectedNode;
			Article article = this.db.Articles[node.Text];
			HashSet<Tag> articleTags = new HashSet<Tag>(article.ArticleTags.Select(at => at.Tag));

			var siblingArticles = this.db.Articles.Values
				.Where(a => a != article)
				.Select(a => new {A=a, C=GetCorrelation(articleTags, a)})
				.OrderByDescending(c => c.C)
				.Take(50)
				.ToList();
			this.textBoxArticleGraphDetails.Text = article.Title  + " <" + article.ArticleTags.Count + ">:" + Environment.NewLine +
				string.Join(Environment.NewLine, siblingArticles.Select(c => " " + c.C.ToString().PadLeft(3) + " " + c.A.Title+ " <" + c.A.ArticleTags.Count + ">").ToArray());
		}
		
		#endregion Article graph
						
		#region Tag graph

		private HashSet<Tag> graphedTags = new HashSet<Tag>();
		private HashSet<Tag> rootedGraphedTagss = new HashSet<Tag>();
		private TreeNode showTagGraph(Tag tag)
		{
			if (rootedGraphedTagss.Contains(tag))
			{
				return null;
			}
			TreeNode node = this.treeViewTagGraph.Nodes.Add(tag.Name);
			graphedTags.Add(tag);
			rootedGraphedTagss.Add(tag);
			HashSet<Article> articleTags = new HashSet<Article>(tag.ArticleTags.Select(at => at.Article));

			List<Tag> siblings = this.db.Tags.Values
				.Where(a => !graphedTags.Contains(a))
				.OrderByDescending(a => GetTagCorrelation(articleTags, a))
				.Take(5)
				.ToList();
			foreach (Tag sibling in siblings)
			{
				node.Nodes.Add(sibling.Name);
				graphedTags.Add(sibling);
			}
			node.Expand();
			return node;
		}

		private static int GetTagCorrelation(HashSet<Article> tagArticles, Tag testedTag)
		{
			int nbCommonTag = testedTag.ArticleTags.Select(at => at.Article).Where(a => tagArticles.Contains(a)).Count();
			return nbCommonTag;
		}

		private void treeViewTagGraph_DoubleClick(object sender, EventArgs e)
		{
			TreeNode node = this.treeViewTagGraph.SelectedNode;
			if (node.Parent != null)
			{
				Tag tag = this.db.Tags[node.Text];
				TreeNode newNode = showTagGraph(tag);
				if (newNode != null)
				{
					this.treeViewTagGraph.SelectedNode = newNode;
				}
			}
		}

		private void initTagGraph()
		{
			List<Tag> top100 = this.db.Tags.Values
					.OrderByDescending(t => t.ArticleTags.Count)
					.Take(100).ToList();
			HashSet<Article> tagArticles = new HashSet<Article>();
			for (int i = 0; i < 7; i++)
			{
				Tag tag = top100
					.OrderBy(t => GetTagCorrelation(tagArticles, t))
					.First();
				showTagGraph(tag);
				tagArticles.UnionWith(tag.ArticleTags.Select(at => at.Article));
			}
		}

		private void treeViewTagGraph_AfterSelect(object sender, TreeViewEventArgs e)		
		{
			TreeNode node = this.treeViewTagGraph.SelectedNode;
			Tag tag = this.db.Tags[node.Text];
			HashSet<Article> tagArticles = new HashSet<Article>(tag.ArticleTags.Select(at => at.Article));

			var siblingTags = this.db.Tags.Values
				.Where(t => t != tag)
				.Select(t => new { T = t, C = GetTagCorrelation(tagArticles, t) })
				.OrderByDescending(c => c.C)
				.Take(50)
				.ToList();
			this.textBoxTagGraphDetails.Text = tag.Name + " <" + tag.ArticleTags.Count + ">:" + Environment.NewLine +
				string.Join(Environment.NewLine, siblingTags.Select(c => " " + c.C.ToString().PadLeft(3) + " " + c.T.Name + " <" + c.T.ArticleTags.Count + ">").ToArray());
		}

		#endregion Tag graph

		#region Tag Tree

		private void initTagTree()
		{
			TagTree tagTree = new TagTree(this.db);
            foreach (TagTreeNode dataNode in tagTree.Root)
            {
                treeViewTagTreeAdd(this.treeViewTagTree.Nodes, dataNode);
            }			
		}

        private void treeViewTagTreeAdd(TreeNodeCollection parent, TagTreeNode dataNode)
        {
            TreeNode guiNode = parent.Add(dataNode.ToString());
            guiNode.Tag = dataNode;

            foreach (TagTreeNode childDataNode in dataNode.GetChildren().OrderByDescending(g => g.InnnerTagsCount))
            {
                treeViewTagTreeAdd(guiNode.Nodes, childDataNode);
            }
        }

		private void treeViewTagTree_AfterSelect(object sender, TreeViewEventArgs e)
		{
            TagTreeNode dataNode = (TagTreeNode)this.treeViewTagTree.SelectedNode.Tag;
            this.textBoxTagTreeDetails.Text = dataNode.GetDebugDisplay(this.db);

		}

		#endregion Tag Tree

	}
}
