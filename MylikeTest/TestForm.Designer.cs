﻿namespace Mylike
{
    partial class TestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.ReportTagButton = new System.Windows.Forms.Button();
            this.tagReportButton = new System.Windows.Forms.Button();
            this.computationResultTextBox = new System.Windows.Forms.TextBox();
            this.nextButton = new System.Windows.Forms.Button();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.textBoxArticleGraphDetails = new System.Windows.Forms.TextBox();
            this.treeViewArticleGraph = new System.Windows.Forms.TreeView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.textBoxTagGraphDetails = new System.Windows.Forms.TextBox();
            this.treeViewTagGraph = new System.Windows.Forms.TreeView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.textBoxTagTreeDetails = new System.Windows.Forms.TextBox();
            this.treeViewTagTree = new System.Windows.Forms.TreeView();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1335, 641);
            this.tabControl1.TabIndex = 6;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.ReportTagButton);
            this.tabPage1.Controls.Add(this.tagReportButton);
            this.tabPage1.Controls.Add(this.computationResultTextBox);
            this.tabPage1.Controls.Add(this.nextButton);
            this.tabPage1.Controls.Add(this.dataGridView);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1327, 615);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Tags liked";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // ReportTagButton
            // 
            this.ReportTagButton.Location = new System.Drawing.Point(757, 230);
            this.ReportTagButton.Name = "ReportTagButton";
            this.ReportTagButton.Size = new System.Drawing.Size(42, 23);
            this.ReportTagButton.TabIndex = 10;
            this.ReportTagButton.Text = "View";
            this.ReportTagButton.UseVisualStyleBackColor = true;
            this.ReportTagButton.Click += new System.EventHandler(this.ReportTagButton_Click);
            // 
            // tagReportButton
            // 
            this.tagReportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tagReportButton.Location = new System.Drawing.Point(805, 580);
            this.tagReportButton.Name = "tagReportButton";
            this.tagReportButton.Size = new System.Drawing.Size(134, 23);
            this.tagReportButton.TabIndex = 9;
            this.tagReportButton.Text = "Recompute tags";
            this.tagReportButton.UseVisualStyleBackColor = true;
            this.tagReportButton.Click += new System.EventHandler(this.tagReportButton_Click);
            // 
            // computationResultTextBox
            // 
            this.computationResultTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.computationResultTextBox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.computationResultTextBox.Location = new System.Drawing.Point(805, 6);
            this.computationResultTextBox.Multiline = true;
            this.computationResultTextBox.Name = "computationResultTextBox";
            this.computationResultTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.computationResultTextBox.Size = new System.Drawing.Size(514, 568);
            this.computationResultTextBox.TabIndex = 8;
            // 
            // nextButton
            // 
            this.nextButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.nextButton.Location = new System.Drawing.Point(676, 580);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(75, 23);
            this.nextButton.TabIndex = 7;
            this.nextButton.Text = "Next";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // dataGridView
            // 
            this.dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView.Location = new System.Drawing.Point(6, 6);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.Size = new System.Drawing.Size(745, 568);
            this.dataGridView.TabIndex = 6;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.textBoxArticleGraphDetails);
            this.tabPage2.Controls.Add(this.treeViewArticleGraph);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1327, 615);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Article graph";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // textBoxArticleGraphDetails
            // 
            this.textBoxArticleGraphDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxArticleGraphDetails.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxArticleGraphDetails.Location = new System.Drawing.Point(558, 6);
            this.textBoxArticleGraphDetails.Multiline = true;
            this.textBoxArticleGraphDetails.Name = "textBoxArticleGraphDetails";
            this.textBoxArticleGraphDetails.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxArticleGraphDetails.Size = new System.Drawing.Size(761, 601);
            this.textBoxArticleGraphDetails.TabIndex = 1;
            // 
            // treeViewArticleGraph
            // 
            this.treeViewArticleGraph.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.treeViewArticleGraph.Location = new System.Drawing.Point(8, 6);
            this.treeViewArticleGraph.Name = "treeViewArticleGraph";
            this.treeViewArticleGraph.Size = new System.Drawing.Size(473, 601);
            this.treeViewArticleGraph.TabIndex = 0;
            this.treeViewArticleGraph.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterSelect);
            this.treeViewArticleGraph.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.treeView_MouseDoubleClick);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.textBoxTagGraphDetails);
            this.tabPage3.Controls.Add(this.treeViewTagGraph);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1327, 615);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Tag graph";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // textBoxTagGraphDetails
            // 
            this.textBoxTagGraphDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTagGraphDetails.Location = new System.Drawing.Point(565, 12);
            this.textBoxTagGraphDetails.Multiline = true;
            this.textBoxTagGraphDetails.Name = "textBoxTagGraphDetails";
            this.textBoxTagGraphDetails.Size = new System.Drawing.Size(754, 595);
            this.textBoxTagGraphDetails.TabIndex = 1;
            // 
            // treeViewTagGraph
            // 
            this.treeViewTagGraph.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.treeViewTagGraph.Location = new System.Drawing.Point(8, 12);
            this.treeViewTagGraph.Name = "treeViewTagGraph";
            this.treeViewTagGraph.Size = new System.Drawing.Size(515, 595);
            this.treeViewTagGraph.TabIndex = 0;
            this.treeViewTagGraph.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeViewTagGraph_AfterSelect);
            this.treeViewTagGraph.DoubleClick += new System.EventHandler(this.treeViewTagGraph_DoubleClick);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.textBoxTagTreeDetails);
            this.tabPage4.Controls.Add(this.treeViewTagTree);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1327, 615);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Tag tree";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // textBoxTagTreeDetails
            // 
            this.textBoxTagTreeDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTagTreeDetails.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTagTreeDetails.Location = new System.Drawing.Point(691, 19);
            this.textBoxTagTreeDetails.Multiline = true;
            this.textBoxTagTreeDetails.Name = "textBoxTagTreeDetails";
            this.textBoxTagTreeDetails.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxTagTreeDetails.Size = new System.Drawing.Size(628, 573);
            this.textBoxTagTreeDetails.TabIndex = 1;
            // 
            // treeViewTagTree
            // 
            this.treeViewTagTree.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.treeViewTagTree.Location = new System.Drawing.Point(9, 4);
            this.treeViewTagTree.Name = "treeViewTagTree";
            this.treeViewTagTree.Size = new System.Drawing.Size(602, 603);
            this.treeViewTagTree.TabIndex = 0;
            this.treeViewTagTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeViewTagTree_AfterSelect);
            // 
            // TestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1335, 641);
            this.Controls.Add(this.tabControl1);
            this.Name = "TestForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Test";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.Button ReportTagButton;
		private System.Windows.Forms.Button tagReportButton;
		private System.Windows.Forms.TextBox computationResultTextBox;
		private System.Windows.Forms.Button nextButton;
		private System.Windows.Forms.DataGridView dataGridView;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TreeView treeViewArticleGraph;
		private System.Windows.Forms.TextBox textBoxArticleGraphDetails;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.TextBox textBoxTagGraphDetails;
		private System.Windows.Forms.TreeView treeViewTagGraph;
		private System.Windows.Forms.TabPage tabPage4;
		private System.Windows.Forms.TreeView treeViewTagTree;
        private System.Windows.Forms.TextBox textBoxTagTreeDetails;

	}
}

