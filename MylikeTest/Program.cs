﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

namespace Mylike
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
			try
			{
				Application.EnableVisualStyles();
				Application.SetCompatibleTextRenderingDefault(false);
				var form = new TestForm();
				form.Init();
				Application.Run(form);
			}
			catch (Exception ex)
			{
				Trace.WriteLine(ex);
				MessageBox.Show(ex.ToString());
			}
        }
    }
}
