﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models
{
	public class AddArticleViewModel
	{
		[Required]
		[Url]
		[Display(Name = "URL")]
		public string Url { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Title")]
		[MaxLength(128)]
		public string Title { get; set; }

		
		
	}
}