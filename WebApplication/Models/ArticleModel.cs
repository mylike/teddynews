﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{
	/// <summary>
	/// For Partiel Render of _Article.cshtml (the div in witch there is an article)	
	/// </summary>
	public class ArticleModel
	{
		public Article Article;
		public Opinion Opinion;
		public bool CanBeClosed;
		

		public int Id
		{
			get { return this.Article.Id; }
		}
		public string Title
		{
			get { return this.Article.Title; }
		}
		public string Host
		{
			get 
			{
				try
				{
					string host = new Uri(this.Article.Url).Host;
					if (host.ToLower().StartsWith("www."))
					{
						return host.Substring(4);
					}
					return host;
				}
				catch (UriFormatException)
				{
					return "Error";
				}
			}
		}




		public string Score
		{
			get { return this.Article.InitialRating.ToString("+0;-0"); }
		}

		public string Clock
		{
			get 
			{
				TimeSpan span = DateTime.UtcNow.Subtract(this.Article.DateTimeStamp);
				int nbHour = (int) Math.Round(span.TotalHours);
				int nbDays = span.Days;
				if (nbHour <= 1)
				{
					return "one hour";
				}
				if (nbHour < 10)
				{
					return nbHour + " hours";
				}
				if (nbDays <= 1)
				{
					return "one day";
				}
				if (nbDays <= 5)
				{
					return nbDays + " days";
				}
				int nbWeek = nbDays / 7;
				if (nbWeek <= 1)
				{
					return "one week";
				}
				if (nbWeek <= 6)
				{
					return "one month";
				}
				return this.Article.DateTimeStamp.ToString("MMMyy");
			}
		}

		public override string ToString()
		{
			return this.Id + " " + this.Title;
		}

		
	}
}