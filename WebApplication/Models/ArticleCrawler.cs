﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{
	public class ArticleCrawler
	{
		public ArticleCrawler()
		{

		}

		public string url { get; set; }
		public string source { get; set; }
		public string score { get; set; }
		public string count { get; set; }
		public string title { get; set; }
		public string urlComments { get; set; }
		
	}
}