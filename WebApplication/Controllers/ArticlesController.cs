﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Common;
using WebApplication.Models;
using Newtonsoft.Json.Linq;
using log4net;
using System.IO;
using Newtonsoft.Json;
using System.Data.Entity.Validation;

namespace WebApplication.Controllers
{
	/// <summary>
	/// API for the spider crawler.
	/// </summary>
    public class ArticlesController : ApiController
    {
        private Model db = new Model();
		private static ILog logger = LogManager.GetLogger(typeof(ArticlesController));

		[HttpGet]
		public IHttpActionResult CrawlerNotification(string name, string jobid)
		{
			try
			{
				if (string.IsNullOrEmpty(name) || string.IsNullOrWhiteSpace(jobid) || name == "None" || jobid == "None")
				{
					string err = "Invalid parameter : name=" + name + " - jobid=" + jobid;
					logger.Error(err);
					return Ok(err);
				}

				WebClient client = new WebClient();
				string url = string.Format("http://scrapy-aureliendepalma.rhcloud.com/items/mylike/{0}/{1}.jl", name, jobid);
				string line = null;
				StreamReader reader = new StreamReader(client.OpenRead(url));

				logger.Debug("Loading " + url);

				while ((line = reader.ReadLine()) != null)
				{
					try
					{

						ArticleCrawler item = JsonConvert.DeserializeObject<ArticleCrawler>(line);

						if (item == null)
						{
							logger.Error("ArticleCrawler is null (json=" + line + ")");
							continue;
						}
						new Uri(item.url);///check format

						Article article = db.Articles.FirstOrDefault(a => a.Url == item.url);

						if (article == null)
						{
							
							article = new Article
							{
								Title = item.title,
								Url = item.url,								
								DateTimeStamp = DateTime.UtcNow,
							};
							ArticlesController.AddTag(db, article, name);
							db.Articles.Add(article);
						}
						if (string.IsNullOrEmpty(article.UrlComment))
						{
							article.UrlComment = item.urlComments;
						}
						article.InitialRating = int.Parse(item.score);

						db.SaveChanges();
					}
					catch (DbEntityValidationException dbEx)
					{
						logger.Error("Error during inserton of article : " + line + " => " + dbEx);
						foreach (var validationErrors in dbEx.EntityValidationErrors)
						{
							foreach (var validationError in validationErrors.ValidationErrors)
							{
								logger.Error("Property:" + validationError.PropertyName + ", Error: " + validationError.ErrorMessage);
							}
						}
					}
					catch(Exception ex)
					{
						logger.Error("Error during inserton of article : " + line + " => " + ex);
					}
				}

				return Ok(url);
			}
			catch (Exception ex)
			{
				logger.Error("Error loading file : " + ex);
			}

			return Ok("Error");
		}

        // GET: api/Articles
       /* public IQueryable<Article> GetArticles()
        {
            return db.Articles;
        }

	   
        // GET: api/Articles/5
        [ResponseType(typeof(Article))]
        public IHttpActionResult GetArticle(int id)
        {
            Article article = db.Articles.Find(id);
            if (article == null)
            {
                return NotFound();
            }

            return Ok(article);
        }

        // PUT: api/Articles/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutArticle(int id, Article article)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != article.Id)
            {
                return BadRequest();
            }

            db.Entry(article).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ArticleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Articles
        [ResponseType(typeof(Article))]
        public IHttpActionResult PostArticle(Article article)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Articles.Add(article);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = article.Id }, article);
        }*/

        // DELETE: api/Articles/5
        /*[ResponseType(typeof(Article))]
        public IHttpActionResult DeleteArticle(int id)
        {
            Article article = db.Articles.Find(id);
            if (article == null)
            {
                return NotFound();
            }

            db.Articles.Remove(article);
            db.SaveChanges();

            return Ok(article);
        }
		*/

		/// curl -v -H "Content-Type: application/json" -X POST -d @hn1.txt http://localhost:50727/api/Articles

		//// POST: api/Articles
		//[ResponseType(typeof(void))]
		//public IHttpActionResult PostArticle([FromBody] ArticleCrawler[] articles)
		//{
		//	if (!ModelState.IsValid)
		//	{
		//		return BadRequest(ModelState);
		//	}

		//	foreach (ArticleCrawler item in articles)
		//	{
		//		try
		//		{
		//			Article article = db.Articles.FirstOrDefault(a => a.Url == item.url);

		//			if (article == null)
		//			{
		//				article = new Article
		//				{
		//					Title = item.title,
		//					Url = item.url,							
		//					DateTimeStamp = DateTime.Today,
		//				};				
						
		//				ArticlesController.AddTag(db, article, item.source);
		//				Uri url = new Uri(item.url);
		//				ArticlesController.AddTag(db, article, url.Host);
		//				db.Articles.Add(article);
		//			}

		//			article.InitialRating = int.Parse(item.score);

		//			db.SaveChanges();
		//		}
		//		catch(Exception ex)
		//		{
		//			Console.WriteLine(ex);
		//		}
		//	}

			

		//	return StatusCode(HttpStatusCode.NoContent);
		//}

		//private static int getRatingForHN(int hnNote)
		//{
		//	if (hnNote > 5000)
		//	{
		//		return 10;
		//	}
		//	if (hnNote > 2000)
		//	{
		//		return 9;
		//	}
		//	if (hnNote > 1000)
		//	{
		//		return 8;
		//	}
		//	if (hnNote > 500)
		//	{
		//		return 7;///Target: have one article per day at 7
		//	}
		//	if (hnNote > 250)
		//	{
		//		return 6;
		//	}
		//	if (hnNote > 120)
		//	{
		//		return 5;
		//	}
		//	if (hnNote > 50)
		//	{
		//		return 4;
		//	}
		//	if (hnNote > 20)
		//	{
		//		return 3;
		//	}
		//	if (hnNote > 10)
		//	{
		//		return 2;
		//	}
		//	if (hnNote >= 4)
		//	{
		//		return 1;
		//	}
		//	return 0;
		//}
		public static void AddTag(Model db, Article newArticle, string tagName)
		{
			Tag tag = db.Tags.FirstOrDefault(t => t.Name == tagName);
			if (tag == null)
			{
				tag = new Tag { Name = tagName };
				logger.Info("Create tag " + tagName);
			}			
			newArticle.ArticleTags.Add(new ArticleTag { Tag = tag });
		}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ArticleExists(int id)
        {
            return db.Articles.Count(e => e.Id == id) > 0;
        }
    }
}