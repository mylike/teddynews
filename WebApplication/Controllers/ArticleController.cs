﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Common;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using WebApplication.Models;



namespace WebApplication.Controllers
{
	/// <summary>
	/// Main controller of the TeddyNews
	/// </summary>
	public class ArticleController : TeddyNewsController
	{
		private static log4net.ILog log = log4net.LogManager.GetLogger(typeof(ArticleController));


		public async Task<ActionResult> Index()
		{
			Account account = await this.getAccount();
			if (account == null)
			{
				return RedirectToAction("Index", "About");
			}
			log.Info("Create " + this.GetType().Name + " for " + Request.UserHostAddress + ", account: " + account);

			await GreedyReco.CheckViewTime(db, account);
			await GreedyReco.RefreshCurrentArticles(db, account);

			///Load AccountCurrentArticles in memory
			var r = await db.AccountCurrentArticles
				.Where(c => c.AccountId == account.Id)
				.OrderBy(c => c.Id)
				.Include(c => c.Article.ArticleTags)
				.Select(c => new { A = c.Article, O = c.Article.Opinions.FirstOrDefault(o => o.AccountId == account.Id) })
				.ToListAsync();
			List<ArticleModel> articleList = r
				.Select(i => new ArticleModel { Article = i.A, Opinion = i.O, CanBeClosed = true })
				.ToList();			
			ViewBag.ArticleList = articleList;
			return View();
		}
		public async Task<ActionResult> Test()
		{
			Account account = await this.getAccount();
			if (account == null)
			{
				return RedirectToAction("Index", "About");
			}
			log.Info("Create " + this.GetType().Name + " for " + Request.UserHostAddress + ", account: " + account);

			await GreedyReco.RefreshCurrentArticles(db, account);

			///Load AccountCurrentArticles in memory
			var r = await db.AccountCurrentArticles
				.Where(c => c.AccountId == account.Id)
				.OrderBy(c => c.Id)
				.Include(c => c.Article.ArticleTags)
				.Select(c => new { A = c.Article, O = c.Article.Opinions.FirstOrDefault(o => o.AccountId == account.Id) })
				.ToListAsync();
			List<ArticleModel> articleList = r
				.Select(i => new ArticleModel { Article = i.A, Opinion = i.O, CanBeClosed = true })
				.ToList();
		
			ViewBag.ArticleList = articleList;
			return View();
		}

		public async Task<ActionResult> Visited()
		{
			Account account = await this.getAccount();
			if (account == null)
			{
				return RedirectToAction("Index", "About");
			}
			log.Info("Create " + this.GetType().Name + " for " + Request.UserHostAddress + ", account: " + account);

			var r = await db.Opinions
				.Where(c => c.AccountId == account.Id)
				.OrderByDescending(c => c.Id)
				.Include(c => c.Article.ArticleTags)
				.Take(128)
				.Select(c => new { A = c.Article, O = c.Article.Opinions.FirstOrDefault(o => o.AccountId == account.Id) })
				.ToListAsync();
			List<ArticleModel> articleList = r
				.Select(i => new ArticleModel { Article = i.A, Opinion = i.O, CanBeClosed = false })
				.ToList();
			ViewBag.ArticleList = articleList;
			return View();
		}
		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Skip(int id)
		{
			Account account = await this.getAccount();
			if (account == null)
			{
				return RedirectToAction("Index");
			}

			AccountCurrentArticle current = account.AccountCurrentArticles.Where(c => c.ArticleId == id).FirstOrDefault();
			if (current != null)
			{
				db.Entry(current).State = EntityState.Deleted;
				await db.SaveChangesAsync();
			}
			Opinion opinion = await this.getOrCreateOpinion(account, id);
			opinion.Skipped = true;
			await GreedyReco.OpinionUpdated(db, opinion);
			await db.SaveChangesAsync();
			return RedirectToAction("Index");
		}


		
		[HttpPost]
		public async Task<ActionResult> SkipThankAjax()
		{
			string articleIdStr = Request.Form["articleId"];
			string thankStr = Request.Form["thank"];
			if (string.IsNullOrEmpty(articleIdStr))
			{
				return Content("<!-- articleId null -->");
			}
			Account account = await this.getAccount();
			if (account == null)
			{
				return Content("<!-- account null -->");
			}

			int articleId = int.Parse(articleIdStr);
			AccountCurrentArticle current = account.AccountCurrentArticles.Where(c => c.ArticleId == articleId).FirstOrDefault();
			if (current != null)
			{
				db.Entry(current).State = EntityState.Deleted;
				await db.SaveChangesAsync();
			}
			Opinion opinion = await this.getOrCreateOpinion(account, articleId);
			opinion.Skipped = true;
			if (thankStr == "1")
			{
				opinion.Visited = true;
			}
			await GreedyReco.OpinionUpdated(db, opinion);
			await db.SaveChangesAsync();

			///Get new article
			await GreedyReco.RefreshCurrentArticles(db, account);

			///Load AccountCurrentArticles in memory
			var r = await db.AccountCurrentArticles
				.Where(c => c.AccountId == account.Id)
				.OrderByDescending(c => c.Id)
				.Include(c => c.Article.ArticleTags)
				.Select(c => new { A = c.Article, O = c.Article.Opinions.FirstOrDefault(o => o.AccountId == account.Id) })
				.FirstOrDefaultAsync();
			ArticleModel articleModel = new ArticleModel { Article = r.A, Opinion = r.O, CanBeClosed = true };

			return PartialView("_Article", articleModel);

			//return Content("<div>ok for " + articleId + " </div>");
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Next()
		{
			Account account = await getAccount();
			if (account == null)
			{
				return RedirectToAction("Index");
			}

			foreach (AccountCurrentArticle currentArticle in account.AccountCurrentArticles.ToList())
			{
				Opinion opinion = await this.getOrCreateOpinion(account, currentArticle.ArticleId);
				opinion.Skipped = true;
				await GreedyReco.OpinionUpdated(db, opinion);
				db.Entry(currentArticle).State = EntityState.Deleted;
			}
			await db.SaveChangesAsync();
			return RedirectToAction("Index");
		}


	
		public async Task<ActionResult> Redirect(int id)
		{
			Article article = await db.Articles.FirstOrDefaultAsync(a => a.Id == id);
			if (article == null)
			{
				throw new HttpException(404, "Article " + id + " not found");
			}
			Account account = await this.getAccount();
			if (account != null)
			{
				Opinion opinion = await this.getOrCreateOpinion(account, id);
				opinion.Visited = true;
				await GreedyReco.OpinionUpdated(db, opinion);
				await db.SaveChangesAsync();
			}
			return Redirect(article.Url);
		}


		

		public ActionResult Add()
		{
			return View();
		}


		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Add(AddArticleViewModel model)
		{
			if (ModelState.IsValid)
			{
				Account account = await this.getAccount();
				Article article = db.Articles.FirstOrDefault(a => a.Url == model.Url);

				if (article == null)
				{
					article = new Article
					{
						Title = model.Title,
						Url = model.Url,
						DateTimeStamp = DateTime.UtcNow,
					};
					ArticlesController.AddTag(db, article, "User");
					article.InitialRating = account != null ? 5 : 1;
					db.Articles.Add(article);
				}
				db.SaveChanges();				
				return RedirectToAction("Index", "Article");

			}
			// If we got this far, something failed, redisplay form
			return View(model);
		}
	}


}