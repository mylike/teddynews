﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Microsoft.AspNet.Identity;

namespace WebApplication.Controllers
{
	/// <summary>
	/// Conveniantes tools for Controller that use DB.
	/// </summary>
	public abstract class TeddyNewsController : Controller
	{
		private static log4net.ILog log = log4net.LogManager.GetLogger(typeof(TeddyNewsController));


		protected Model db;

		public TeddyNewsController()
		{			
			db = new Model();
			db.Configuration.LazyLoadingEnabled = false;
		}
		
		protected async Task<Account> getAccount()
		{
			Guid accountId;
			if (User.Identity.IsAuthenticated)
			{
				accountId = Guid.Parse(User.Identity.GetUserId());
			}
			else
			{
				HttpCookie accountCookie = HttpContext.Request.Cookies["AccountId"];
				if (accountCookie == null)
				{
					return null;
				}
				if (!Guid.TryParse(accountCookie.Value, out accountId))
				{
					return null;
				}
			}
			Account account = await db.Accounts
				.Include(a => a.AccountTastes.Select(at => at.Tag))
				.Include(a => a.AccountCurrentArticles)
				.FirstOrDefaultAsync(a => a.Id == accountId);
			if (account == null && User.Identity.IsAuthenticated)
			{
				log.Info("Recreate account for authenticated user " + accountId);
				account = await this.createAccount();
			}
			if (account != null)
			{
				if (account.IsAuthenticated && !User.Identity.IsAuthenticated)
				{
					await this.deleteAccountCookie();
					return null;
				}
			}
			return account;
		}
		protected async Task deleteAccountCookie()
		{
			HttpCookie accountCookie = HttpContext.Request.Cookies["AccountId"];
			if (accountCookie != null)
			{
				Guid accountId;
				if (Guid.TryParse(accountCookie.Value, out accountId))
				{
					Account account = await db.Accounts
						.Include(a => a.AccountCurrentArticles)
						.Include(a => a.AccountTastes)
						.Include(a => a.Opinions)
						.FirstOrDefaultAsync(a => a.Id == accountId);
					if (account != null && !account.IsAuthenticated)
					{
						account.AccountCurrentArticles.ToList().ForEach(r => db.Entry(r).State = EntityState.Deleted);
						account.AccountTastes.ToList().ForEach(r => db.Entry(r).State = EntityState.Deleted);
						account.Opinions.ToList().ForEach(r => db.Entry(r).State = EntityState.Deleted);						
						await db.SaveChangesAsync();
						account = await db.Accounts
							.FirstOrDefaultAsync(a => a.Id == accountId);
						db.Entry(account).State = EntityState.Deleted;
						await db.SaveChangesAsync();
					}
				}
			}

			HttpCookie myCookie = new HttpCookie("AccountId");
			myCookie.Expires = DateTime.Now.AddDays(-10);
			Response.Cookies.Add(myCookie);
		}
		protected async Task<Account> tryCreateAccount()
		{
			Account existingAccount = await this.getAccount() ;
			if (existingAccount != null)
			{
				return existingAccount;///already done
			}
			return await createAccount();
		}

		private async Task<Account> createAccount()
		{
			Guid accountId;
			if (User.Identity.IsAuthenticated)
			{
				accountId = Guid.Parse(User.Identity.GetUserId());
				log.Info("Create login account " + accountId);
				await this.deleteAccountCookie();
			}
			else
			{
				accountId = Guid.NewGuid();
				log.Info("Create cookie account " + accountId);
				var accountCookie = new HttpCookie("AccountId", accountId.ToString());
				accountCookie.Expires = DateTime.Now.AddMonths(6);
				HttpContext.Response.Cookies.Add(accountCookie);
			}
			Account account = new Account();
			account.Id = accountId;
			account.CurrentArticleDate = DateTime.UtcNow;
			account.IsAuthenticated = User.Identity.IsAuthenticated;
			db.Accounts.Add(account);
			await db.SaveChangesAsync();
			return account;
		}

		public static async Task CreateAccountForNewLogin(Guid accountId)
		{
			using (var db = new Model())
			{
				db.Configuration.LazyLoadingEnabled = false;
				Account oldAccount = await db.Accounts
					.FirstOrDefaultAsync(a => a.Id == accountId);
				if (oldAccount != null)
				{
					return;
				}
				Account account = new Account();
				account.Id = accountId;
				account.CurrentArticleDate = DateTime.UtcNow;
				account.IsAuthenticated = true;
				db.Accounts.Add(account);
				await db.SaveChangesAsync();
			}
		}

		protected async Task<Opinion> getOrCreateOpinion(Account account, int articleId)
		{
			Opinion opinion = await db.Opinions.FirstOrDefaultAsync(o => o.AccountId == account.Id && o.ArticleId == articleId);
			if (opinion == null)
			{
				opinion = new Opinion { Account = account, ArticleId = articleId };
				db.Opinions.Add(opinion);
			}
			return opinion;
		}

		protected override void OnException(ExceptionContext filterContext)
		{
			
			log.Fatal(filterContext.Exception);
			base.OnException(filterContext);
		}
	}
}