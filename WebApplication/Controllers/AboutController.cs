﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Common;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using WebApplication.Models;

namespace WebApplication.Controllers
{	
	public class AboutController : TeddyNewsController
	{
		private static log4net.ILog log = log4net.LogManager.GetLogger(typeof(AboutController));


		public ActionResult Index()
		{			
			return View();
		}

		[HttpPost]
		public async Task<ActionResult> Start()
		{
			await this.tryCreateAccount();
			return RedirectToAction("Index", "Article");
		}

		[HttpPost]
		public async Task<ActionResult> DeleteCookie()
		{
			
			await this.deleteAccountCookie();
			return RedirectToAction("Index");
		}

		
		
	}
}