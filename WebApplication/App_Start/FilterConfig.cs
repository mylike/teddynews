﻿using System.Web;
using System.Web.Mvc;

namespace WebApplication
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }

    //public class EnsureCookie : ActionFilterAttribute
    //{
    //    public override void OnActionExecuting(ActionExecutingContext filterContext)
    //    {
    //        var accountIdCookie = filterContext.HttpContext.Request.Cookies["AccountId"];
    //        if (accountIdCookie == null)
    //        {
               
    //            // cookie doesn't exist, either pull preferred lang from user profile
    //            // or just setup a cookie with the default language
    //            accountIdCookie = new HttpCookie("AccountId", "en-gb");
    //            filterContext.HttpContext.Request.Cookies.Add(accountIdCookie);
    //        }
    //        // do something with langCookie
    //        base.OnActionExecuting(filterContext);
    //    }
    //}
}
